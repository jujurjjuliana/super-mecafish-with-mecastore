public enum EFishType { SPEED, TANK }

public interface IFishData
{
	string Name { get; }
	//int ID { get; }
	int Number { get; }
	EFishType Type { get; }
	IUpgradeCollection Upgrades { get; }
}

public interface IUpgradeCollection
{
	int One { get; }
	int Two { get; }
	int Three { get; }
	
	void Evolve( int id );
}


public class Upgrades : IUpgradeCollection
{
	public int One { get; private set; }
	public int Two { get; private set; }
	public int Three { get; private set; }
	
	public void Evolve( int id )
	{
		if( !CanEvolve( id ) )
			return;
		
		if( id == 0 && One < 3 ) One++;
		if( id == 1 && Two < 3 ) Two++;
		if( id == 2 && Three < 3 ) Three++;
	}
	
	public bool CanEvolve( int id )
	{
		if( id == 0 ) return One < 3;
		if( id == 1 ) return Two < 3;
		if( id == 2 ) return Three < 3;
		return false;
	}
}


public abstract class FishData: IFishData
{
	public abstract EFishType Type { get; }	
	public abstract string Name { get; }
	public int Number { get; private set; }
	
	Upgrades _upgrades = new Upgrades();
	public IUpgradeCollection Upgrades { get { return _upgrades; } }
	
	public FishData( int number )
	{
		this.Number = number;
	}
}

public class TankFishData : FishData
{
	public override EFishType Type { get { return EFishType.TANK; } }
	public override string Name { get { return "Tank Fish " + Number; } }
	
	public TankFishData( int number ) : base( number ) { }
}

public class SpeedFishData : FishData
{
	public override EFishType Type { get { return EFishType.SPEED; } }
	public override string Name { get { return "Speed Fish " + Number; } }
	
	public SpeedFishData( int number ) : base( number ) { }
}