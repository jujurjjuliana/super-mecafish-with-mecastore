﻿using UnityEngine;
using System.Collections;

public class PlayEffect : MonoBehaviour {

	public bool Play;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Play) {
			if (!GetComponent<ParticleSystem> ().isPlaying) {
				GetComponent<ParticleSystem> ().Play ();
			}
			Play = false;
		}
	}
}
