﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface INatureData 
{
	List<Enemy> EnemyList { get; }
	List<Enemy> CollectableList { get; }
}

public class NatureData  : INatureData 
{
	public List<Enemy> EnemyList { get; private set; }
	public List<Enemy> CollectableList { get; private set; }

	public NatureData (List<Enemy> EnemyList,List<Enemy> CollectableList)
	{
		this.EnemyList = EnemyList;
		this.CollectableList = CollectableList;
	}

}
