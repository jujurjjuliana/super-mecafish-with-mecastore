﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Sdkbox;

/// <summary>
/// Store user interface item. Holds the variables of the item in the store UI
/// </summary>
public class PremiumStoreUiItem : MonoBehaviour {

	[SerializeField] Button buyButton;
	[SerializeField] Text priceText;
	[SerializeField] string _price;
	[SerializeField] string _productName;

	public string Price{get{return _price;} set{_price = value; ShowPrice ();}}
	public string ProductName{get{return _productName;}}

	public void ShowPrice(){
		priceText.text = _price;

	}





}