﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Sdkbox;

/// <summary>
/// Store user interface controller.
/// It controls everything that happens in the Store UI.
/// </summary>
public class StoreUiController : MonoBehaviour {


	[SerializeField] StoreUiItem[] items;
	[SerializeField] PremiumStoreUiItem[] premiumItems;
	[SerializeField] Text confirmPrice;
	[SerializeField] GameObject ConfirmPanel;
	[SerializeField] GameObject NotEnoughCoins;


	[SerializeField] Button _itemsButton;
	[SerializeField] Button _coinsButton;
	[SerializeField] Color _normalButtonColor;	
	[SerializeField] Color _selectedButtonColor;




	[SerializeField] Sprite _selectedSprite;
	[SerializeField] Sprite _selectSprite;

	[SerializeField] GameObject _itemsPanel;
	[SerializeField] GameObject _coinsPanel;
	[SerializeField] GameObject _coinsPanelFacebook;

	public Sprite SelectedSprite {get { return _selectedSprite; }}
	public Sprite SelectSprite {get { return _selectSprite; }}

	int confirmIndex;

//	void OnEnable(){
//		UpdateUi ();
//	}

	/// <summary>
	/// Updates the user interface of the store.
	/// </summary>
	public void UpdateUi(){
		
		if (_itemsPanel.activeSelf) {
			_itemsButton.image.color = _selectedButtonColor;
			_coinsButton.image.color = _normalButtonColor;
			UpdateItemsPanel ();
		} else {
			_coinsButton.image.color = _selectedButtonColor;
			_itemsButton.image.color = _normalButtonColor;
			UpdateCoinsPanel ();
		}
	}

	/// <summary>
	/// Updates the items panel.
	/// </summary>
	public void UpdateItemsPanel(){
		int selectedSkin = GameSave.GetSelectedSkin();
		for (int i = 0; i < items.Length; i++) {
			items [i].SetBought (GameSave.GetBoughtSkin (i) >= 0);
			items [i].SetSelected (selectedSkin == i);
		}
		//items [0].SetBought (true);
		GameStateController.Instance.Ui.UpdateCoins ();
	}
	/// <summary>
	/// Updates the coins panel.
	/// </summary>
	public void UpdateCoinsPanel(){
		for (int i = 0; i < premiumItems.Length; i++) {
			premiumItems [i].ShowPrice ();
		}
		GameStateController.Instance.Ui.UpdateCoins ();
	}

	/// <summary>
	/// Selects the skin.
	/// </summary>
	/// <param name="index">Index.</param>
	//TODO: It is used to select equiped items, not Skins. It shouls be renamed.
	public void SelectSkin(int index){
		if (GameSave.GetSelectedSkin () == index) {
			GameSave.SetSelectedSkin (-1);
		} else {
			GameSave.SetSelectedSkin (index);
		}
		UpdateItemsPanel ();

	}

	/// <summary>
	/// This event is called when the buy button is pushed. Confirmation Dialog is then shown if the player has enough coins.
	/// </summary>
	/// <param name="index">Index.</param>

	public void ClickBuyEvent(int index){
		if (GameSave.getCoins () >= items [index].Price) {
			ShowConfirm (index);
		} else {
			ShowNotEnoughCoins ();
		}
	}

	/// <summary>
	/// Shows the not enough coins dialog.
	/// </summary>
	public void ShowNotEnoughCoins(){
		NotEnoughCoins.SetActive (true);
	}
	/// <summary>
	/// Hides the not enough coins dialog.
	/// </summary>
	public void HideNotEnoughCoins(){
		NotEnoughCoins.SetActive (false);
	}

	/// <summary>
	/// Shows the confirm dialog.
	/// </summary>
	/// <param name="index">Index.</param>
	public void ShowConfirm(int index){
		confirmPrice.text = "x " + items [index].Price + " ?";
		ConfirmPanel.SetActive (true);
		confirmIndex = index;
	}
	/// <summary>
	/// Confirms the purchase.
	/// </summary>
	public void ConfirmedBuy(){
		BuyItem (confirmIndex);
		HideConfirm ();

	}
	/// <summary>
	/// Purchase is canceled.
	/// </summary>
	public void CancelBuy(){
		HideConfirm ();
	}
	public void HideConfirm(){
		ConfirmPanel.SetActive (false);
	}

	/// <summary>
	/// When cofirmation is done, the item is finally bought. This function applies discounts the money and adds the item.
	/// </summary>
	/// <param name="index">Index.</param>
	void BuyItem(int index){
		//its bought already
		if (GameSave.GetBoughtSkin (index) >= 0)
			return;
		int price = items [index].Price;
		if (GameSave.getCoins () >= price) {
			GameSave.SetBoughtSkin(index,price);
			GameSave.setCoins(GameSave.getCoins()-price);
			UpdateItemsPanel ();
		}
	}

	/// <summary>
	/// Enables the items panel.
	/// </summary>
	public void EnableItemsPanel(){
		_itemsPanel.SetActive (true);
		#if UNITY_WEBGL
			_coinsPanelFacebook.SetActive (false);
		#else
			_coinsPanel.SetActive (false);
		#endif
		UpdateUi ();
	}

	/// <summary>
	/// Enables the coins panel.
	/// </summary>
	public void EnableCoinsPanel(){
		#if UNITY_WEBGL
			_coinsPanelFacebook.SetActive (true);
		#else
			_coinsPanel.SetActive (true);
		#endif
		_itemsPanel.SetActive (false);
		UpdateUi ();
	}

	/// <summary>
	/// Updates the price of the coins based on the IAP store.
	/// This function is called by the IAP sdk
	/// </summary>
	/// <param name="products">Products.</param>
	public void UpdatePriceBasedOnStore(Product[] products){
		foreach (Product product in products) {
			foreach (PremiumStoreUiItem item in premiumItems) {				
				if (product.name.Equals (item.ProductName)) {
					item.Price = product.price;
					DebugHelper.Log ("The product " + product.name + " price was updated in store. New price is " + product.price);
				}
			}

		}
	}

}
