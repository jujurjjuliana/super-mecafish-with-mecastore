﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// Stage panel. Controls the variables in the stage panel in the level selection panel
/// </summary>
public class StagePanel : MonoBehaviour {

	[SerializeField] Text stageNumber;
	[SerializeField] Text score;
	[SerializeField] GameObject locked;

	public void setStageNumber(int number){
		stageNumber.text = number.ToString ();
	}

	public void setScore(int newScore){
		score.text = newScore.ToString ();
	}

	public void setLocked(bool isLocked){
		locked.SetActive (isLocked);
		GetComponent<Button> ().interactable = !isLocked;
	}

	public void setStageInfo(int number, int newScore, bool isLocked){
		setStageNumber (number);
		setScore (newScore);
		setLocked (isLocked);
		GetComponent<Button> ().interactable = !isLocked;
	}
}
