﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// Store user interface item. Holds the variables of the item in the store UI
/// </summary>
public class StoreUiItem : MonoBehaviour {

	[SerializeField] Button buyButton;
	[SerializeField] Button selectionButton;
	[SerializeField] Text priceText;
	[SerializeField] Text selectionText;
	[SerializeField] int _price;

	public int Price{get{return _price;}}

	public void SetBought(bool isBought){
		if (isBought) {
			buyButton.gameObject.SetActive (false);
			selectionButton.gameObject.SetActive (true);
		} else {
			buyButton.gameObject.SetActive (true);
			selectionButton.gameObject.SetActive (false);
			priceText.text = Price.ToString ();
		}
	}
	public void SetSelected(bool isSelected){
		if (selectionButton != null) {
			if (isSelected) {
				selectionButton.image.sprite = GameStateController.Instance.Ui.StoreUiController.SelectedSprite;
				selectionText.text = "Remove";
				//selectionButton.interactable = false;
			} else {
				selectionButton.image.sprite = GameStateController.Instance.Ui.StoreUiController.SelectSprite;
				selectionText.text = "Select";
				//selectionButton.interactable = true;
			}
		}
	}


}