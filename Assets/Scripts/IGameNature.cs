﻿using UnityEngine;
using System.Collections;

public interface IGameNature 
{
	void SetData(INatureData  natureData);
	IGameEventBroadcaster  Events { get; }

	int OilCount{ get;}
	void Clear();
	int QtdOilRemoved { get;}
}
