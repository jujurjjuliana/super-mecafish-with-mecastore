using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class DragManager
{

	Dictionary< int, DragGesture > _currentDrags = new Dictionary<int, DragGesture>();
    SubscribersCollection<System.Func<DragGesture, bool>> _listeners = new SubscribersCollection<System.Func<DragGesture, bool>>();
    List<int> _toRemove = new List<int>();

	public SubscribersCollection<System.Func<DragGesture, bool>> DragEvents { get { return _listeners; } }
	
	public bool IsNecessary { get { return _listeners.HasSubscribers; } }
	
	public void UncheckedAll()
	{
		_toRemove.Clear();

		_toRemove.AddRange( _currentDrags.Keys );
	}
	
	public List<DragGesture> VerifyUnchecked()
	{
		var ret = ( from d in _toRemove select _currentDrags[ d ] ).ToList();
		foreach( var key in _toRemove )
		{
			CompleteDrag( key );
		}

		return ret;
	}
	
	public void CompleteMouseDrag() { CompleteDrag( 0 ); }
	void CompleteDrag( int dragId )
	{
		_currentDrags[ dragId ].CompleteGesture();
		_currentDrags.Remove( dragId );
	}


	void NewDragEvent( int dragId, DragGesture gesture )
	{
		_currentDrags[ dragId ] = gesture;

		foreach( var listener in _listeners.Subscribers )
		{
			if(listener.Invoke(gesture))
			{						
				break;
			}
		}
	}
	
	public void NotifyMouseDrag( Vector2 position ) { NotifyDrag( 0, position ); }

	public void NotifyDrag( int fingerId, Vector2 position )
	{
		int id = FingerIdToDragId( fingerId );
		DragGesture gest;
		if( _currentDrags.TryGetValue( id, out gest ) )
		{

			gest.AddPoint( position );
		}
		else
		{
			NewDragEvent( id, new DragGesture( position ) );
		}
		
		_toRemove.Remove( id );

	}
	
	int FingerIdToDragId( int fingerId ) { return fingerId + 1; }
	int DragIdToFingerId( int dragId ) { return dragId - 1;	}
	

	public class DragGesture
	{
		public event System.Action<DragGesture> OnDragComplete = (obj) => { };
		public event System.Action<DragGesture> OnDrag = (obj) => { };
		
		List<TimedPosition> _points;
		
		public bool IsComplete { private set; get; }
		public IEnumerable<TimedPosition> Points { get { return _points; } }
		
		public float LifeTime { get { return Last.Time - First.Time; } }
		public TimedPosition First { get { return _points.First(); } }
		public TimedPosition Last { get { return _points.Last(); } }
		public Vector2 FirstPoint { get { return First.Position; } }
		public Vector2 LastPoint { get { return Last.Position; } }
		
		public float Distance { get { return Vector2.Distance( LastPoint, FirstPoint ); } }

		public int totalPoints;

		public DragGesture( Vector2 beginPoint )
		{
			_points = new List<TimedPosition>{ new TimedPosition( beginPoint ) };
		}


		#region Damn C#
		public void Simplify()
		{
			_points = new List<TimedPosition>{ First, Last };
		}
		
		internal void AddPoint( Vector2 point )
		{
			if( point != LastPoint )
			{
				_points.Add( new TimedPosition( point ) );				

				OnDrag( this );
			
			}
		}
		
		internal void CompleteGesture()
		{
			IsComplete = true;
			_points.Add( new TimedPosition( LastPoint ) );
			OnDragComplete( this );
		}

		public void CountPoints()
		{
			totalPoints = _points.Count ();
		}

		#endregion Damn C#
	}
}
