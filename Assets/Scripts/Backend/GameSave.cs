﻿using UnityEngine;
using System;
using System.Collections.Generic;

/// <summary>
/// Game save. Monoton
/// This class controls variables that need to be store between sessions.
/// It controls the number of coins the player have, the items he acquires,
/// the items that are equiped and the highscores of each Level.
/// </summary>
public class GameSave {


	private static GameSave _i; // The monotonic instance of the class GameSave
	public static int numOfItems = 6; // The total number of items in the game, for iteration purposes
	private int coins; //Number of coins the player have
	private int clearedLevels; //Number of levels the player cleared until the present

	/// <summary>
	/// Initializes a new instance of the <see cref="GameSave"/> class.
	/// </summary>
	private GameSave(){

		if(!PlayerPrefs.HasKey("version")){

			PlayerPrefs.SetString("version",Application.version);
			PlayerPrefs.Save();
		}
		PlayerPrefs.SetString("version",Application.version);

		coins = PlayerPrefs.HasKey("coins")? PlayerPrefs.GetInt("coins") : 0;
		clearedLevels = PlayerPrefs.HasKey("clearedLevels")? PlayerPrefs.GetInt("clearedLevels") : 0;

	}

	/// <summary>
	/// Load local game values
	/// </summary>
	public static void load(){
		_i = new GameSave();
	}

	/// <summary>
	/// Save local game values that are stores in instance variables
	/// </summary>
	private void save(){
		PlayerPrefs.SetInt("coins", coins);
		PlayerPrefs.SetInt("clearedLevels", clearedLevels);

		PlayerPrefs.Save();
	}

	/// <summary>
	/// Clear the local save. Used mostly for debuggin purposes.
	/// </summary>
	public static void clear(){
		PlayerPrefs.DeleteAll();
		load ();
	}
		

	#region Getters

	public static int getCoins(){ return _i.coins;}
	public static int getClearedLevels(){ return _i.clearedLevels;}
	public static int GetScore(int lvl){return PlayerPrefs.HasKey("lvl_"+lvl.ToString())? PlayerPrefs.GetInt("lvl_"+lvl.ToString()) : 0;	}
	public static int GetBoughtSkin(int index){return PlayerPrefs.HasKey("skin_"+index.ToString())? PlayerPrefs.GetInt("index_"+index.ToString()) : -1;	}
	public static int GetSelectedSkin(){return PlayerPrefs.HasKey("selectedSkin")? PlayerPrefs.GetInt("selectedSkin") : -1;	}
	public static bool GetTutorial(){return PlayerPrefs.HasKey ("tutorial");}

	#endregion



	#region Setters

	public static void setCoins(int val){ _i.coins = val; _i.save();}
	public static void setClearedLevels(int val){ _i.clearedLevels = val; _i.save();}
	public static void SetScore(int lvl, int score){ PlayerPrefs.SetInt("lvl_"+lvl.ToString(), score);	PlayerPrefs.Save();	}
	public static void SetBoughtSkin(int index, int price){ PlayerPrefs.SetInt("skin_"+index.ToString(), price);	PlayerPrefs.Save();	}
	public static void SetSelectedSkin(int index){ PlayerPrefs.SetInt("selectedSkin", index);	PlayerPrefs.Save();	}
	public static void AddCoins(int val){ _i.coins += val; _i.save();}
	public static void SetTutorial(){ PlayerPrefs.SetInt("tutorial",1);	PlayerPrefs.Save();	}

	#endregion
}
