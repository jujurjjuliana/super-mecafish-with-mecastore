﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// PlayerDataSync. Monoton
/// This class is intended to deal with all sincronization calls to our unified database.
/// It will handle the calls to the database
/// </summary>
public class PlayerDataSync : MonoBehaviour {

	public static PlayerDataSync _instance;

	public static PlayerDataSync Instance{
		get{
			return _instance;
		}
	}
	#if UNITY_WEBGL && UNITY_EDITOR
	string url_mecadata = "https://www.supermecafish.com/ClientData/meca_data.php?"; 
	#else //temporarily using unsecure protocol for android
	string url_mecadata = "http://www.supermecafish.com/ClientData/meca_data.php?"; 
	#endif
	string pass = "mecapass";
	public string player_id;
	public bool new_player = true;
	public bool intialized = false;
	public string fb_id;
	public string player_name;
	public string player_email;

	/// <summary>
	/// Once the facebook is connected, and the the player info is succesfully retrieved
	/// we are able to initialize the data sincronization with the database using the players facebook id.
	/// Other information is also sent in this same request to make it quicker.
	/// </summary>
	/// <param name="facebook_id">Facebook identifier.</param>
	/// <param name="name">Name.</param>
	/// <param name="email">Email.</param>
	public void initilize(string facebook_id,string name, string email){
		fb_id = facebook_id;
		player_name = name;
		player_email = email;
		//web requests are Async, so a coroutin is used to wait for the results
		StartCoroutine ("initilizeCoroutine");
	}

	// Use this for initialization
	void Start () {
		player_id = "";
		new_player = true;
		intialized = false;
		fb_id = "";
	}

	void Awake(){
		_instance = this;
	}

	// Update is called once per frame
	void Update () {
	
	}

	/// <summary>
	/// The PHP script returns a string with variables assigned as variable_id=variable_value
	/// and uses & as separator for the variables. This function will return a dictionary
	/// Dictionary<string, string>, where the first string is the varibale identifier and the second
	/// is that variables value.
	/// </summary>
	/// <returns>The parser.</returns>
	/// <param name="phpresult">Phpresult.</param>
	Dictionary<string, string> SimpleParser(string phpresult){
		Dictionary<string, string> dictionary =	new Dictionary<string, string>();
		string[] vars = phpresult.Split ('&');
		foreach (string variavel in vars) {
			string[] splitted = variavel.Split ('=');
			dictionary.Add (splitted [0].Trim(), splitted [1].Trim());
		}
		return dictionary;
	}
		
	/// <summary>
	/// This coroutine will make the initialization of the player data syncronization. It will make a request
	/// via php using WWW, and wait for the results to apply in the game.
	/// </summary>
	/// <returns>The coroutine.</returns>
	IEnumerator initilizeCoroutine() {
		DebugHelper.Log ("Sincronizaçao inicial começou");
		GameStateController.Instance.Ui.SyncingScreen.SetActive (true);
		string url = url_mecadata + "check_player=1&pass=" + pass + "&fb_id=" + fb_id + "&user_name=" + player_name + "&user_email=" + player_email;
		DebugHelper.Log (url);
		WWW www = new WWW(url);
		yield return www;
		Dictionary<string, string> vars = SimpleParser(www.text);
		player_id = vars ["player_id"];
		new_player = vars ["player_exists"].Equals ("0");
		SetItems (vars ["items_id"], vars ["items_qtt"]);
		intialized = true;
		DebugHelper.Log ("Sincronizaçao inicial concluída");

		GameStateController.Instance.Ui.SyncingScreen.SetActive (false);
	}

	/// <summary>
	/// Receive 2 strings containing logical arrays of integers separated by #
	/// The strings are ordered in a way that the quantity in items_qtts[i] corresponds to the
	/// the item with id in items_ids[i]. This function set the items quantities in the game.
	/// </summary>
	/// <param name="items_ids">Items identifiers.</param>
	/// <param name="items_qtts">Items qtts.</param>
	void SetItems(string items_ids,string items_qtts){
		string[] ids = items_ids.Split ('#');
		string[] qtts = items_qtts.Split ('#');
		for (int i = 0; i < ids.Length; i++) {
			int ids_i = int.Parse (ids [i].Trim ());
			int qtts_i = int.Parse (qtts [i].Trim ());
			if (ids_i == 1) {
				GameSave.setCoins (qtts_i);
			} else {
				if (qtts_i > 0) {
					GameSave.SetBoughtSkin (ids_i - 2, qtts_i);
				}
			}
		}
	}

	/// <summary>
	/// Gets the information about items and creates a string passing 2 arrays in the php sintaxe.
	/// items_id[] and items_qtt[] in a order where the player has items_qtt[i] units of the item
	/// with id items_id[i]
	/// </summary>
	/// <returns>The items.</returns>
	public string GetItems(){
		string output = "";
		output += "&items_id[]=" + 1;
		output += "&items_qtt[]=" + GameSave.getCoins();
		for(int i = 0; i < GameSave.numOfItems; i++){
			if (GameSave.GetBoughtSkin (i) >= 0) {
				output += "&items_id[]=" + (i + 2);
				output += "&items_qtt[]=" + 1;
			}
		}
		Debug.Log (output);
		return output;
	}


}
