﻿using UnityEngine;
using System.Collections;
using Sdkbox;
using UnityEngine.UI;

/// <summary>
/// IAP manager.
/// This class is intended to controll the flow of IAP actions and
/// perform the correct changes in the game.
/// </summary>
public class IAPManager : MonoBehaviour {

	[SerializeField] IAP iap;
	public Text logField;

	// Use this for initialization
	void Start () {
		//Its implemented only for android
		#if !UNITY_WEBGL
		iap.gameObject.SetActive(true);
		#endif
	}
	
	// Update is called once per frame
	void Update () {

	}

	/// <summary>
	/// Se o IAP retornar sucesso em uma compra. O id da compra é identificado e o valor é 
	/// If the IAP returns success in a purchase. The id of the purchase is identified and
	/// the correct actions are taken.
	/// </summary>
	/// <param name="product">Product.</param>
	public void OnSuccess(Product product){
		if (product.id == "coin_pack_small") {
			DebugHelper.Log ("Player succesfully bought 2500 coins");
			GameSave.AddCoins (2500);
			GameStateController.Instance.Ui.UpdateCoins ();
		}
		if (product.id == "coin_pack_medium") {
			DebugHelper.Log ("Player succesfully bought 8000 coins");
			GameSave.AddCoins (8000);
			GameStateController.Instance.Ui.UpdateCoins ();
		}
	}
	/// <summary>
	/// If the IAP return a request error, it is logged.
	/// </summary>
	/// <param name="error">Error.</param>
	public void onProductRequestFailure (string error){
		DebugHelper.Log (error);
	}
	/// <summary>
	/// If the IAP return a product purchase cancelation, this function takes the correct measures.
	/// </summary>
	/// <param name="product">Product.</param>
	public void onCanceled (Product product){
		DebugHelper.Log ("The product " + product.name + " buy was cancelled.");

	}
	/// <summary>
	/// Called after initialization, and having requested the products from the store using the keys specified for each platform. 
	/// If the request was successful, then this callback is invoked with the array of available products.
	/// </summary>
	/// <param name="products">Products.</param>
	public void onProductRequestSuccess (Product[] products){
		foreach (Product product in products) {
			DebugHelper.Log ("The product " + product.name + " request was successful.");
		}

	}

}
