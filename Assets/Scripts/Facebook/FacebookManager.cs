﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using System;
using UnityEngine.UI;

/// <summary>
/// Facebook manager.
/// This class manages the FB api calls in the way that the game needs.
/// It is recommended to call this class methods intead of the FB api
/// this clas will interact with th FB api.
/// </summary>
public class FacebookManager : MonoBehaviour {

	private static FacebookManager _instance;
	public bool IsLoggedIn{get;set;}
	public string ProfileName{get;set;}
	public Sprite ProfilePic{get;set;}
	public bool Initializing{ get; set;}
	public Text logField;
	public string AppLinkURL =  "https://fb.me/265614557111843";

	public static FacebookManager Instance{
		get{
			if (_instance == null) {
				GameObject fbm = new GameObject ("FBManager");
				fbm.AddComponent<FacebookManager> ();
			}
			return _instance;
		}
	}



	void Awake(){
		DontDestroyOnLoad (this.gameObject);
		_instance = this;
		Initializing = true;
	}

	/// <summary>
	/// Initialize the FB api
	/// </summary>
	public void InitFB(){

		if (!FB.IsInitialized) {
			// Initialize the Facebook SDK
			FB.Init(SetInit,OnHideUnity);
		} else {
			// Already initialized, signal an app activation App Event
			FB.ActivateApp();
			IsLoggedIn = FB.IsLoggedIn;
		}
	}
	//FB api Initialization callback
	private void SetInit ()
	{
		
		if (FB.IsLoggedIn) {
			IsLoggedIn = FB.IsLoggedIn;
			DebugHelper.Log ("FB is logged in");
			GetUserEmail ();
			//FacebookManager.Instance.GetUserData ();
			//GetProfile ();
		} else {
			DebugHelper.Log ("FB is not logged in");
		}
		return;
		IsLoggedIn = FB.IsLoggedIn;
		Initializing = false;

	}
	//FB api Initialization callback
	private void OnHideUnity (bool isGameShown)
	{

		if (!isGameShown) {
			// Pause the game - we will need to hide
			Time.timeScale = 0;
		} else {
			// Resume the game - we're getting focus again
			Time.timeScale = 1;
		}
	}


	void DisplayUsername(IResult result){
		if (result.Error != null) {
			DebugHelper.Log(result.Error);
		} else {
			ProfileName = "" + result.ResultDictionary ["first_name"];
		}
	}

	void DisplayProfilePic(IGraphResult result){
		if (result.Texture != null) {
			ProfilePic = Sprite.Create (result.Texture, new Rect (0, 0, 128, 128), new Vector2 ());
		} else {
			DebugHelper.Log ("No picture was returned");
		}
	}

	/// <summary>
	/// Shares a link with a message. This is here for reference.
	/// </summary>
	public void ShareGame(){
		FB.FeedShare (
			string.Empty,
			new Uri (AppLinkURL),
			"Play Super Mecafish",
			"Save the ocean",
			"I'll have fun, and save the world!",
			new Uri ("http://vangelissimeonidis.com/wp-content/uploads/2015/03/under-construction.jpg"),
			string.Empty,
			ShareGameCallBack
		);
	}
	void ShareGameCallBack(IResult result){
		if (result.Cancelled) {
			DebugHelper.Log ("Share Canceled.");
		} else if (!string.IsNullOrEmpty (result.Error)) {
			DebugHelper.Log ("Error on share!");
		} else if (!string.IsNullOrEmpty (result.RawResult)) {
			DebugHelper.Log ("success on share");
		}
	}

	/// <summary>
	/// Invite to play the game. This is here for reference.
	/// </summary>
	public void Invite(){
		FB.Mobile.AppInvite (
			new Uri(AppLinkURL),
			new Uri ("http://vangelissimeonidis.com/wp-content/uploads/2015/03/under-construction.jpg"),
			InviteCallback
		);
	}
	void InviteCallback(IResult result){
		if (result.Cancelled) {
			DebugHelper.Log("Invite Canceled.");
		} else if (!string.IsNullOrEmpty (result.Error)) {
			DebugHelper.Log("Error on invite!");
		} else if (!string.IsNullOrEmpty (result.RawResult)) {
			DebugHelper.Log("success on invite");
		}
	}

	/// <summary>
	/// Takes a screenshot and share to facebook. This is used to share the score of a Level.
	/// </summary>
	private IEnumerator TakeScreenshot() 
	{
		yield return new WaitForEndOfFrame();

		var width = Screen.width;
		var height = Screen.height;
		var tex = new Texture2D(width, height, TextureFormat.RGB24, false);
		// Read screen contents into the texture
		tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
		tex.Apply();
		byte[] screenshot = tex.EncodeToPNG();

		var wwwForm = new WWWForm();
		wwwForm.AddBinaryData("image", screenshot, "Screenshot.png");
		wwwForm.AddField ("name", "Look at this score! Think you can beat me? Come play Super Mecafish with me!");

		FB.API("me/photos", HttpMethod.POST, TakeSSCallback, wwwForm);
	}
	void TakeSSCallback(IResult result){
		if (result.Error != null) {
			DebugHelper.Log(result.Error);
		} else {
			DebugHelper.Log ("Succeeded posting SS");
		}
	}
	public void ShareSS(){
		StartCoroutine ("TakeScreenshot");
	}


	string emailUrl;
	/// <summary>
	/// Gets the user email via facebook and send to a php script that stores it
	/// </summary>
	public void GetUserEmail(){
		if (FacebookManager.Instance.IsLoggedIn) {
			// Make a Graph API call to get email address
			DebugHelper.Log ("Starting email Request");   

			FB.API ("/me?fields=email", HttpMethod.GET, graphResult => {
				if (string.IsNullOrEmpty (graphResult.Error) == false) {
					DebugHelper.Log ("could not get email address");
					return;
				}

				string email = graphResult.ResultDictionary ["email"] as string;
				string id = graphResult.ResultDictionary ["id"] as string;
				DebugHelper.Log("email is : " +email);
				#if UNITY_WEBGL && UNITY_EDITOR
				emailUrl = "https://www.supermecafish.com/ClientData/mecamail.php?email="+email;
				#else //temporarily using unsecure protocol for android
				emailUrl = "http://www.supermecafish.com/ClientData/mecamail.php?email="+email;
				#endif

				StartCoroutine("SaveEmail");


			});
		} else {
			DebugHelper.Log("Not logged in to get email");
		}
	}

	/// <summary>
	/// Gets the user data from facebook. Then calls PlayerDataSync initialisation using that info.
	/// </summary>
	public void GetUserData(){
		if (FacebookManager.Instance.IsLoggedIn) {
			GameStateController.Instance.Ui.SyncingScreen.SetActive (true);
			// Make a Graph API call to get email address
			FB.API ("/me?fields=email,first_name,last_name", HttpMethod.GET, graphResult => {
				GameStateController.Instance.Ui.SyncingScreen.SetActive (false);
				if (string.IsNullOrEmpty (graphResult.Error) == false) {
					DebugHelper.Log ("could not complete FB query");
					return;
				}
				string email = graphResult.ResultDictionary ["email"] as string;
				string id = graphResult.ResultDictionary ["id"] as string;
				string name = graphResult.ResultDictionary ["first_name"] as string;
				GameStateController.Instance.Ui.SyncingScreen.SetActive (false);
				//PlayerDataSync.Instance.initilize(id,name,email);

			});
		} else {
			DebugHelper.Log("Not logged in to get email");
		}
	}

	/// <summary>
	/// Tries to send the email to the php script
	/// </summary>
	/// <returns>The email.</returns>
	IEnumerator SaveEmail() {
		int tries = 3;
		WWW www;
		do{
			DebugHelper.Log ("Enviando tentativa: " + (4-tries));
			www = new WWW(emailUrl);
			yield return www;
			if(--tries < 0){
				yield break;
			}
		}while(!string.IsNullOrEmpty(www.error));

		yield return www;
		if(string.IsNullOrEmpty(www.error)){
			DebugHelper.Log ("Enviado para:  " + www.url);
		}
	}

}
