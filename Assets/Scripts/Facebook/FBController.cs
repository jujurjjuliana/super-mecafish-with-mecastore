﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using System;
using UnityEngine.UI;

/// <summary>
/// FB controller.
/// This class is intended to handle the facebook request actions. It is an interface to the FacebookManager, a class that manages the
/// direct contact to the FB api
/// </summary>
public class FBController : MonoBehaviour {

	public GameObject loggedIn;
	public GameObject loggedOut;
	public Text ProfileName;
	public Image profilePic;
	public bool IsWaitingAuth = false;
	public bool AutoConnectFacebook = false;


	public Text logField;

	// Use this for initialization
	void Awake () {
		initialize ();
	}


	//Initialize the facebook using the FacebookManager
	public void initialize(){
		FacebookManager.Instance.InitFB ();
		if (logField)
			FacebookManager.Instance.logField = logField;

		#if UNITY_WEBGL && !UNITY_EDITOR
		if(AutoConnectFacebook){
			StartCoroutine("TryToLogInFacebook");
		}
		#endif
	//	handleMenus (FacebookManager.Instance.IsLoggedIn);
	//	Log("is FB logged? " + FB.IsLoggedIn);
	//	FacebookManager.Instance.logField = logField;
	//	StartCoroutine ("WaitForProfileInitialize");
	}




	//Log in facebook with basic permissions.
	public void login(){

		if (!FB.IsLoggedIn) {
			var perms = new List<string> (){ "public_profile", "email", "user_friends" };
			FB.LogInWithReadPermissions (perms, AuthCallback);
			GameStateController.Instance.Ui.SyncingScreen.SetActive (true);
		}

	}
	//Authorization Callback for the login
	private void AuthCallback (ILoginResult result) {
		GameStateController.Instance.Ui.SyncingScreen.SetActive (false);
		if (result.Error != null) {
			Log (result.Error);
		} else if (result.Cancelled) {
			StopCoroutine ("TryToLogInFacebook");
		}

		if (FB.IsLoggedIn) {
			// AccessToken class will have session details
			//var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
			// Print current access token's User ID
			//Debug.Log(aToken.UserId);
			// Print current access token's granted permissions

			//foreach (string perm in aToken.Permissions) {
			//	Debug.Log (perm);
			//}
			FacebookManager.Instance.IsLoggedIn = true;
			FacebookManager.Instance.GetUserEmail ();
			//FacebookManager.Instance.GetUserData ();
			GameStateController.Instance.Ui.UpdateFacebookBlink ();
			//FacebookManager.Instance.GetProfile();


		} else {
			DebugHelper.Log ("User cancelled login");
		}
		IsWaitingAuth = false;
		handleMenus (FacebookManager.Instance.IsLoggedIn);
	}

	/// <summary>
	///  Log in facebook if it is not logged. Log out facebook otherwise
	/// </summary>
	public void SwitchLogInOut(){
		if (FacebookManager.Instance.IsLoggedIn) {
			logout ();
		} else {
			login ();
		}
	}

	/// <summary>
	/// Logout from facebook.
	/// </summary>
	public void logout(){
		FB.LogOut ();
		FacebookManager.Instance.IsLoggedIn = false;
		handleMenus (false);
	}


	// Share a link in users feed
	//This is an example for future use
	public void sharelink(){
		FB.ShareLink(
			new Uri("http://vangelissimeonidis.com/wp-content/uploads/2015/03/under-construction.jpg"),
			callback: ShareCallback
		);
	}
	private void ShareCallback (IShareResult result) {
		if (result.Cancelled || !String.IsNullOrEmpty(result.Error)) {
			Log("ShareLink Error: "+result.Error);
		} else if (!String.IsNullOrEmpty(result.PostId)) {
			// Print post identifier of the shared content
			Log(result.PostId);
		} else {
			// Share succeeded without postID
			Log("ShareLink success!");
		}
	}

	//Handles the details in UI bases in wether or not the user is logged in facebook
	public void handleMenus(bool logged){
		return; //We re not using this panel anymore
		if (logged) {
			loggedIn.SetActive (true);
//			loggedOut.SetActive (false);

			if (FacebookManager.Instance.ProfileName != null) {
				ProfileName.text = FacebookManager.Instance.ProfileName;
			} else {
				ProfileName.text = "Guest";
				StartCoroutine ("WaitForProfileName");
			}
			if (FacebookManager.Instance.ProfilePic != null) {
				profilePic.sprite = FacebookManager.Instance.ProfilePic;
			} else {
				StartCoroutine ("WaitForProfilePic");
			}

		} else {			
			loggedIn.SetActive (false);
//			loggedOut.SetActive (true);
		}
		GameStateController.Instance.Ui.UpdateCoinsPosition ();
	}

	// Share game info to call people to play.
	public void ShareGame(){
		FacebookManager.Instance.ShareGame ();
	}
	// Invite friends directly
	public void Invite(){
		FacebookManager.Instance.Invite ();
	}

	/*~~~~~~~~~~~~~START: Wait results coroutines ~~~~~~~~~~~~~*/
	public IEnumerator WaitForProfileName(){
		while (FacebookManager.Instance.ProfileName == null) {
			yield return null;
		}
		handleMenus (FacebookManager.Instance.IsLoggedIn);
	}
	public IEnumerator WaitForProfilePic(){
		while (FacebookManager.Instance.ProfilePic == null) {
			yield return null;
		}
		handleMenus (FacebookManager.Instance.IsLoggedIn);
	}
	public IEnumerator WaitForProfileInitialize(){
		while (FacebookManager.Instance.Initializing) {
			yield return null;
		}
		handleMenus (FacebookManager.Instance.IsLoggedIn);
	}
	public IEnumerator WaitForAuthToSS(){
		while (IsWaitingAuth) {
			yield return null;
		}
		FacebookManager.Instance.ShareSS ();
	}
	/*~~~~~~~~~~~~~END: Wait results coroutines ~~~~~~~~~~~~~*/

	//Kind of override Debug.log to debug on mobile devices
	void Log(string log){
		if (logField != null) {
			logField.text = logField.text + log + "\n";
		}
		#if UNITY_EDITOR
			Debug.Log (log);
		#endif
	}

	//Share scores by taking a SS from the score panel and posting to user album
	//TODO: Show to user the print and ask for confirmation to post.
	public void ShareScoreSS(){
		IsWaitingAuth = true;
		FB.LogInWithPublishPermissions (
			new List<string>(){"publish_actions"},
			AuthCallback
		);
		StartCoroutine ("WaitForAuthToSS");

	}

	public void GetUserEmail(){
		FacebookManager.Instance.GetUserEmail ();
	}

	/// <summary>
	/// Keeps trying to connect to facebook
	/// </summary>
	public IEnumerator TryToLogInFacebook(){
		DebugHelper.Log ("Started routine to connect.");
		float waitTime = 1;
		while (!FB.IsLoggedIn) {
			DebugHelper.Log ("Trying to login on facebook in " + waitTime + " seconds");	
			yield return new WaitForSeconds (waitTime);
			login ();
			waitTime += 1;
			if (!FB.IsLoggedIn) {
				DebugHelper.Log ("Did not connect. Will try again");
			} else {
				DebugHelper.Log ("Connected.");
			}

		}
	}


}
