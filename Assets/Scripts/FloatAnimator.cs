﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class FloatAnimator
{
    [SerializeField] float _min = float.MinValue;
    [SerializeField] float _max = float.MaxValue;
    [SerializeField] float _acceleration = 60;
    [SerializeField] float _deacceleration = 60;
    [SerializeField] float _maxVelocity = 10;
	EventSlot<float> _reachValue = new EventSlot<float>();

	public FloatAnimator() { }
	public FloatAnimator( float min, float max ) : this( min, max, 60, 60, 10 ) {}
	public FloatAnimator( float min, float max, float accel, float deaccel ) : this( min, max, accel, deaccel, 10 ) {}
	public FloatAnimator( float min, float max, float accel, float deaccel, float maxVelocity ) : this() { _min = min; _max = max; _acceleration = accel; _deacceleration = deaccel; _maxVelocity = maxVelocity; }

    float _velocity;
    float _current = 1080;
    float _desired;
	public float t;

	public IEventRegister<float> OnValueReach { get { return _reachValue; } }

	public float Value { get { return _current; } }
	public float DesiredValue { get { return _desired; } }

	public float CurrentVelocity { get { return _velocity; } }
	public float MaxVelocity { get { return _maxVelocity; } }
    public float Max { get { return _max; } }
    public float Min { get { return _min; } }

    public void Start( float startValue )
    {	
        _current = Mathf.Clamp( startValue, _min, _max );
        _desired = _current;
    }

    public void SetMinimum( float min ) { _min = min; _desired = Mathf.Clamp( _desired, _min, _max ); _current = Mathf.Clamp( _current, _min, _max ); }
    public void SetMaximum( float max ) { _max = max; _desired = Mathf.Clamp( _desired, _min, _max ); _current = Mathf.Clamp( _current, _min, _max ); }

    public void SetDesire( float desired )
    {
        _desired = Mathf.Clamp( desired, _min, _max );
    }

    public void Update( float deltaTime )
    {
        /*float diff = _desired - _current;
		var aVel = _velocity + Mathf.Sign (diff) * _acceleration * deltaTime;

		var dVel = Mathf.Sqrt( Mathf.Abs( 2 * _deacceleration * diff ) );

		var vel = Mathf.Sign( aVel ) * Mathf.Min( Mathf.Abs( aVel ), Mathf.Abs( dVel ) );

		_velocity = Mathf.Clamp( vel, -_maxVelocity, _maxVelocity );

        if( Mathf.Abs( _velocity * deltaTime ) >= Mathf.Abs( diff ) )
        {
            _current = _desired;

            if( _velocity != 0 )
			{
				_reachValue.Trigger( _desired );
                _velocity = 0;
            }
        }
        else
        {
			_current += _velocity * deltaTime;

            if( _current == _desired )
			{
				_reachValue.Trigger( _desired );
                _velocity = 0;
            }
        }*/

		_current = 1080;
    }
}