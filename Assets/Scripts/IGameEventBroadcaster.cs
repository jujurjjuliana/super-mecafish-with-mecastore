﻿using UnityEngine;
using System.Collections;

public interface IGameEventBroadcaster 
{
	IOilEventBroadcaster Oil { get;}

	IGreenEnemyEventBroadcaster GreenEvent { get;}

	IRedEnemyEventBroadcaster RedEvent { get;}

	IBlackEnemyEventBroadcaster BlackEvent { get;}

	IFishEnemyEventBroadcaster FishEvent { get;}

	IMachineRecicleEventBroadcaster MachineEvent { get;}


}

public interface IOilEventBroadcaster
{
	IEventRegister <ILogicOil> Start { get; }
	IEventRegister <ILogicOil> GetOil { get; }
	IEventRegister <ILogicOil> Increase { get; }
}

public interface IOilEventBroadcasterTrigger
{
	void TriggerStart(ILogicOil oil);
	void TriggerGetOil(ILogicOil oil);
	void TriggerIncrease(ILogicOil oil);
}

//Green
public interface IGreenEnemyEventBroadcaster
{
	IEventRegister<IEnemy> FishContact {get;}
}

public interface IGreenEnemyEventBroadcasterTrigger
{
	void TriggerFishContact(IEnemy enemy);
}


//Red
public interface IRedEnemyEventBroadcaster
{
	IEventRegister<IEnemy> FishContact {get;}
}

public interface IRedEnemyEventBroadcasterTrigger
{
	void TriggerFishContact(IEnemy enemy);
}

//Black
public interface IBlackEnemyEventBroadcaster
{
	IEventRegister<IEnemy> FishContact {get;}
}


public interface IBlackEnemyEventBroadcasterTrigger
{
	void TriggerFishContact(IEnemy enemy);
}

//fishEvent
public interface IFishEnemyEventBroadcaster
{
	IEventRegister<int> FishContact {get;}
}


public interface IFishEnemyEventBroadcasterTrigger
{
	void TriggerFishContact(int t);
}


//machineEvent
public interface IMachineRecicleEventBroadcaster
{
	IEventRegister<int> MachineRecicle {get;}
}

public interface IMachineRecicleEventBroadcasterTrigger
{
	void TriggerMachineRecicle(int t);
}