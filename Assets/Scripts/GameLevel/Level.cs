﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Level.
/// This class holds informations about a game level, so it can be instatiated correctly.
/// </summary>
public class Level : MonoBehaviour {

	[SerializeField] List<Enemy> _enemies;
	[SerializeField] List<Enemy> _collectables; //Agora é black enemy, mas depois sera uma classe especifica de coletaveis/lixo
	[SerializeField] int _levelTime;
	[SerializeField] int _neededTrash;

	public int LevelTime{get{return _levelTime; }}
	public int NeededTrash{get{return _neededTrash; }}
	public List<Enemy> EnemiesList{get{return _enemies; }}
	public List<Enemy> Collectables{get{return _collectables; }}



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
