﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class GameRegions : MonoBehaviour 
{
	[SerializeField] Camera _camera;

	//All positions of the enemie's path that are around the deposit
	List<Vector3> _pointsAroundDeposit = new List<Vector3>();
	public List<Vector3> PointsAroundDeposit { get { return _pointsAroundDeposit; } }

	//Start and End points of the Enemy's path
	[SerializeField] GameObject _startRoundPosition;
	[SerializeField] GameObject _endRoundPosition;


	[SerializeField] Rect _oilEntrance;
	[SerializeField] Transform _entrancePosBegin;
	[SerializeField] Transform _entrancePosEnd;

	[SerializeField] Rect _oilStorage;

	[SerializeField] Rect[] _machineRegions;
	[SerializeField] Rect _pathBlockedRegion;


	void Start() 
	{
		if( _camera == null )
			_camera = Camera.main;
	}

	public Rect OceanSpace
	{
		get
		{
			var r1 = _camera.ViewportPointToRay( Vector3.one );
			var r2 = _camera.ViewportPointToRay( Vector3.one - Vector3.right );
			var r3 = _camera.ViewportPointToRay( Vector3.zero );
			var r4 = _camera.ViewportPointToRay( Vector3.one - Vector3.right );

			var p1 = r1.GetWithZ( 0 );
			var p2 = r2.GetWithZ( 0 );
			var p3 = r3.GetWithZ( 0 );
			var p4 = r4.GetWithZ( 0 );
			
			var minX = Mathf.Min( p1.x, p2.x, p3.x, p4.x );
			var maxX = Mathf.Max( p1.x, p2.x, p3.x, p4.x );
			var minZ = Mathf.Min( p1.y, p2.z, p3.y, p4.y );
			var maxZ = Mathf.Max( p1.y, p2.z, p3.y, p4.y );

			return new Rect( minX, minZ, maxX - minX, maxZ - minZ );
		}
	}


	public List<Vector3> RoundRegion
	{
		get
		{
			if( _startRoundPosition == null || _endRoundPosition == null)
				return null;

			float scale = 6;
			var pb = _startRoundPosition.transform.position;
			var pe = _endRoundPosition.transform.position;
			var dir = ( pe - pb ).normalized;
			var cross = Vector3.Cross( dir, Vector3.up );

			var initial = cross;
			float step = 10f;
			PointsAroundDeposit.Clear ();
			for( float a = step; a <= 360; a += step )
			{
				var current = Quaternion.Euler( 0, 0,step ) * initial;

				PointsAroundDeposit.Add (_startRoundPosition.transform.position + current * scale);

				initial = current;
			}

			return PointsAroundDeposit;
		}
	}



	public Rect OilStorageEntrance	{get	{return _oilEntrance;}	}
	public Vector3 EntrancePosBegin	{get	{return _entrancePosBegin.position;}	}
	public Vector3 EntrancePosEnd	{get	{return _entrancePosEnd.position;}	}
	public Rect OilStorageArea	{get	{return _oilStorage;}	}
	public Rect[] MachineRegions	{get	{return _machineRegions;}	}
	public Rect PathBlockedRegion	{get	{return _pathBlockedRegion;}	}

		

	public bool OverEnemy( Vector3 pos )
	{
		pos.y = 10;
		var ray = new Ray( pos, Vector3.down );

		return Physics.Raycast(ray, 10f, 1 << Constants.Layer.GREEN_ENEMY);
	}


	public bool OverBlackEnemy( Vector3 pos )
	{
		pos.y = 10;
		var ray = new Ray( pos, Vector3.down );

		return Physics.Raycast(ray, 10f, 1 << Constants.Layer.BLACK_ENEMY);
	}

	public bool OverRedEnemy( Vector3 pos )
	{
		pos.y = 10;
		var ray = new Ray( pos, Vector3.down );

		return Physics.Raycast(ray, 10f, 1 << Constants.Layer.RED_ENEMY);
	}

	public bool OverGreenEnemy( Vector3 pos )
	{
		pos.y = 10;
		var ray = new Ray( pos, Vector3.down );

		return Physics.Raycast(ray, 10f, 1 << Constants.Layer.GREEN_ENEMY);
	}




	public bool OverBuildings( Vector3 pos )
	{
		pos.y = 10;
		var ray = new Ray(pos, Vector3.down);

		return Physics.Raycast(ray, 10f, 1 << Constants.Layer.BUILDINGS);
	}

	public bool BelowFish(Vector3 pos)
	{
		pos.y = 10;
		var ray = new Ray(pos, Vector3.down);

		return Physics.Raycast(ray, 10f, 1 << Constants.Layer.FISHES);
	}

	void DrawRectangleOnScreen(Rect rect,Color color){
		var a = new Vector3 (rect.xMin, rect.yMin, 0);
		var b = new Vector3 (rect.xMax, rect.yMin, 0);
		var c = new Vector3 (rect.xMin, rect.yMax, 0);
		var d = new Vector3 (rect.xMax, rect.yMax, 0);

		Debug.DrawLine (a,b,color);
		Debug.DrawLine (a,c,color);
		Debug.DrawLine (c,d,color);
		Debug.DrawLine (b,d,color);
	}


	void OnDrawGizmos()
	{

		var rect = OceanSpace;

		DrawRectangleOnScreen (rect, Color.red);


		//InsideOilStorages
		DrawRectangleOnScreen (OilStorageArea, Color.red);

		//OilStorageEntrance

		DrawRectangleOnScreen (_oilEntrance, Color.blue);

		//Region1

		DrawRectangleOnScreen (PathBlockedRegion, Color.red);


		//Bordas
		for (int index = 0; index < _machineRegions.Length; index++) {
			DrawRectangleOnScreen (MachineRegions[index], Color.yellow);
		}


		//Enemies'Path
		if( _startRoundPosition == null || _endRoundPosition == null)
			return;

		float scale = 6;
		var pb = _startRoundPosition.transform.position;
		var pe = _endRoundPosition.transform.position;
		var dir = ( pe - pb ).normalized;
		var cross = Vector3.Cross( dir, Vector3.up );

		var initial = cross;
		float step = 10f;

		for( float a1 = step; a1 <= 360; a1 += step )
		{
			var current = Quaternion.Euler( 0, 0, step) * initial;

			Debug.DrawLine( _startRoundPosition.transform.position + current * scale, _startRoundPosition.transform.position + initial * scale, Color.yellow );

			initial = current;
		}


	}



}

