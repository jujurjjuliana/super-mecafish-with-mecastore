﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface  IObjectiveCollection  : IEnumerable<IObjective >
{
	bool AllDone { get;}
	IObjective  this [int i] {get;}

}


public class ObjectiveCollection  : IObjectiveCollection  
{
	List<IObjective >  _objectives = new List<IObjective >();

	public IEnumerator<IObjective > GetEnumerator() { return  _objectives.GetEnumerator(); } 
	IEnumerator IEnumerable.GetEnumerator () { return GetEnumerator(); }

	public bool AllDone 
	{
		get 
		{
			for( int i = 0; i <  _objectives.Count; i++ )
			{
				var objective =  _objectives[i];
				if( !objective.IsDone )
					return false;
			}
			return true;
		}
	}



	public IObjective  this[int i] { get {return  _objectives[i];} }
	public int Count { get{ return  _objectives.Count;} }
	public void AddRange(IEnumerable<IObjective > objectives) {  _objectives.AddRange(objectives);}
	public void Add(IObjective  objective) { _objectives.Add (objective);}
}
