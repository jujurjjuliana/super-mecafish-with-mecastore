﻿using UnityEngine;
using System.Collections;

public abstract class BaseObjective  : IObjective 
{
	IGameNature    _nature;

	public abstract bool IsDone{ get;}

	public abstract void Register(IGameNature  events);
	public abstract void Unregister(IGameNature  events);

	void IObjective .Start(IGameNature  nature)
	{
		if( nature!=null)
		{
			((IObjective )this).Finish();
		}

		 _nature = nature;
		Register ( nature);
	}

	void IObjective .Finish()
	{
		if( _nature!=null)
		{
			Unregister( _nature);
			 _nature = null;
		}
	}
}
