﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface IGameScenarioControl
{
	IGameScenario Scene {get;}
	void NextScene();
	int SceneNumber { get;}
}

public class GameScenarioControl  : MonoBehaviour, IGameScenarioControl 
{
	[SerializeField] OceanNature _nature;
	[SerializeField] List<Level> _levels;
	List<GameScenario> _scenes;
	/*
	List<GameScenario> _scenes = new List<GameScenario>
	{
		//criar classe que vai conter as listas e chamar essa classe aqui para adicionar elementos dentro dela

		new GameScenario(new NatureData(new List<int> {1,1}, new List<int>{1,20}, new List<EEnemyType>{EEnemyType.BLACK_ENEMY, EEnemyType.RED_ENEMY }, new List<float>{0.3f, 0.3f}), new TimerCondition(60), new GetOilObjective(1)),
		new GameScenario(new NatureData(new List<int> {1,1,1}, new List<int>{1,20,25}, new List<EEnemyType>{EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.RED_ENEMY}, new List<float>{0.3f, 0.3f, 0.3f}), new TimerCondition(60), new GetOilObjective(2)),
		new GameScenario(new NatureData(new List<int> {1,1,1,1}, new List<int>{1,10,20,25}, new List<EEnemyType>{EEnemyType.RED_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.RED_ENEMY}, new List<float>{0.5f, 0.3f, 0.3f, 0.5f}), new TimerCondition(60), new GetOilObjective(2)),
		new GameScenario(new NatureData(new List<int> {1,1,1,1,1}, new List<int>{1,10,20,25,30}, new List<EEnemyType>{EEnemyType.RED_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.RED_ENEMY, EEnemyType.BLACK_ENEMY}, new List<float>{0.5f, 0.3f, 0.3f, 0.5f, 0.0f}), new TimerCondition(60), new GetOilObjective(3)),
		new GameScenario(new NatureData(new List<int> {1,1,1,1,1,1}, new List<int>{1,5,10,15,20,30}, new List<EEnemyType>{EEnemyType.RED_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.RED_ENEMY, EEnemyType.RED_ENEMY, EEnemyType.BLACK_ENEMY}, new List<float>{0.5f, 0.3f, 0.3f, 0.5f, 0.6f, 0.3f}), new TimerCondition(60), new GetOilObjective(3)),
		new GameScenario(new NatureData(new List<int> {1,1,1,1,1,1,1}, new List<int>{1,10,15,20, 25,30, 33}, new List<EEnemyType>{EEnemyType.RED_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.RED_ENEMY,EEnemyType.BLACK_ENEMY,  EEnemyType.RED_ENEMY,  EEnemyType.GREEN_ENEMY }, new List<float>{0.5f, 0.3f, 0.4f, 0.5f, 0.3f, 0.6f, 0.0f}), new TimerCondition(70), new GetOilObjective(7)),
		new GameScenario(new NatureData(new List<int> {1,1,1,1,1,1,1,1}, new List<int>{1,5,10,15,20, 25,30, 33}, new List<EEnemyType>{EEnemyType.RED_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.RED_ENEMY,EEnemyType.BLACK_ENEMY,  EEnemyType.RED_ENEMY,  EEnemyType.GREEN_ENEMY }, new List<float>{0.5f, 0.3f, 0.3f, 0.4f, 0.5f, 0.3f, 0.6f, 0.0f}), new TimerCondition(80), new GetOilObjective(9)),
		new GameScenario(new NatureData(new List<int> {1,1,1,1,1,1,1,1,1}, new List<int>{1,5,10,15,20,22, 25,30, 33}, new List<EEnemyType>{EEnemyType.RED_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.RED_ENEMY,EEnemyType.RED_ENEMY, EEnemyType.BLACK_ENEMY,  EEnemyType.RED_ENEMY,  EEnemyType.GREEN_ENEMY }, new List<float>{0.5f, 0.3f, 0.3f, 0.4f, 0.5f, 0.7f,0.3f, 0.6f, 0.0f}), new TimerCondition(85), new GetOilObjective(9)),
		new GameScenario(new NatureData(new List<int> {1,1,1,1,1,1,1,1,1}, new List<int>{1,5,10,15,20,22, 25,30, 33}, new List<EEnemyType>{EEnemyType.RED_ENEMY,EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.RED_ENEMY,EEnemyType.RED_ENEMY, EEnemyType.BLACK_ENEMY,  EEnemyType.RED_ENEMY,  EEnemyType.GREEN_ENEMY }, new List<float>{0.5f,0.3f, 0.3f, 0.4f, 0.5f, 0.7f,0.3f, 0.6f, 0.0f}), new TimerCondition(88), new GetOilObjective(9)),
		new GameScenario(new NatureData(new List<int> {1,1,1,1,1,1,1,1,1,1}, new List<int>{1,5,3,10,15,20,22, 25,30, 33}, new List<EEnemyType>{EEnemyType.RED_ENEMY,EEnemyType.GREEN_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.RED_ENEMY,EEnemyType.RED_ENEMY, EEnemyType.BLACK_ENEMY,  EEnemyType.RED_ENEMY,  EEnemyType.GREEN_ENEMY }, new List<float>{0.5f, 0.0f, 0.3f, 0.3f, 0.4f, 0.5f, 0.7f,0.3f, 0.6f, 0.0f}), new TimerCondition(88), new GetOilObjective(10)),

		new GameScenario(new NatureData(new List<int> {1,1,1,1,1,1,1,1,1,1,1}, new List<int>{1,5,3,10,15,20,22, 25,30,32, 33}, new List<EEnemyType>{EEnemyType.RED_ENEMY,EEnemyType.GREEN_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.RED_ENEMY,EEnemyType.RED_ENEMY, EEnemyType.BLACK_ENEMY,  EEnemyType.RED_ENEMY,  EEnemyType.BLACK_ENEMY, EEnemyType.GREEN_ENEMY }, new List<float>{0.5f, 0.0f, 0.3f, 0.3f, 0.4f, 0.5f, 0.7f,0.3f, 0.6f, 0.04f, 0.0f}), new TimerCondition(98), new GetOilObjective(12)),
		new GameScenario(new NatureData(new List<int> {1,1,1,1,1,1,1,1,1,1,1,1}, new List<int>{1,2,5,3,10,15,20,22, 25,30,32, 33}, new List<EEnemyType>{EEnemyType.RED_ENEMY, EEnemyType.RED_ENEMY,EEnemyType.GREEN_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.RED_ENEMY,EEnemyType.RED_ENEMY, EEnemyType.BLACK_ENEMY,  EEnemyType.RED_ENEMY,  EEnemyType.BLACK_ENEMY, EEnemyType.GREEN_ENEMY }, new List<float>{0.4f, 0.5f, 0.0f, 0.3f, 0.3f, 0.4f, 0.5f, 0.7f,0.3f, 0.6f, 0.04f, 0.0f}), new TimerCondition(98), new GetOilObjective(12)),
		new GameScenario(new NatureData(new List<int> {1,1,1,1,1,1,1,1,1,1,1,1,1}, new List<int>{8,1,2,5,3,10,15,20,22, 25,30,32, 33}, new List<EEnemyType>{EEnemyType.GREEN_ENEMY,EEnemyType.RED_ENEMY, EEnemyType.RED_ENEMY,EEnemyType.GREEN_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.RED_ENEMY,EEnemyType.RED_ENEMY, EEnemyType.BLACK_ENEMY,  EEnemyType.RED_ENEMY,  EEnemyType.BLACK_ENEMY, EEnemyType.GREEN_ENEMY }, new List<float>{0.0f,0.4f, 0.5f, 0.0f, 0.3f, 0.3f, 0.4f, 0.5f, 0.7f,0.3f, 0.6f, 0.04f, 0.0f}), new TimerCondition(100), new GetOilObjective(13)),
		new GameScenario(new NatureData(new List<int> {1,1,1,1,1,1,1,1,1,1,1,1,1}, new List<int>{8,1,2,5,3,10,15,20,22, 25,30,32, 33}, new List<EEnemyType>{EEnemyType.RED_ENEMY,EEnemyType.RED_ENEMY, EEnemyType.RED_ENEMY,EEnemyType.GREEN_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.RED_ENEMY,EEnemyType.RED_ENEMY, EEnemyType.BLACK_ENEMY,  EEnemyType.RED_ENEMY,  EEnemyType.BLACK_ENEMY, EEnemyType.GREEN_ENEMY }, new List<float>{0.0f,0.4f, 0.5f, 0.0f, 0.3f, 0.3f, 0.4f, 0.5f, 0.7f,0.3f, 0.6f, 0.04f, 0.0f}), new TimerCondition(100), new GetOilObjective(12)),
		new GameScenario(new NatureData(new List<int> {1,1,1,1,1,1,1,1,1,1,1,1,1,1}, new List<int>{8,1,2,5,3,10,16,15,20,22, 25,30,32, 33}, new List<EEnemyType>{EEnemyType.RED_ENEMY,EEnemyType.RED_ENEMY, EEnemyType.RED_ENEMY,EEnemyType.GREEN_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY,  EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.RED_ENEMY,EEnemyType.RED_ENEMY, EEnemyType.BLACK_ENEMY,  EEnemyType.RED_ENEMY,  EEnemyType.BLACK_ENEMY, EEnemyType.GREEN_ENEMY }, new List<float>{0.0f,0.4f, 0.5f, 0.0f, 0.3f, 0.3f,0.6f, 0.4f, 0.5f, 0.7f,0.3f, 0.6f, 0.04f, 0.0f}), new TimerCondition(110), new GetOilObjective(14)),

		/*new GameScenario(new NatureData(new List<int> {1,1,1,1}, new List<int>{1,10,20,25}, new List<EEnemyType>{EEnemyType.RED_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.RED_ENEMY}, new List<float>{0.5f, 0.3f, 0.3f, 0.5f}), new TimerCondition(60), new GetOilObjective(4)),
		new GameScenario(new NatureData(new List<int> {1,1,1,1}, new List<int>{1,10,20,25}, new List<EEnemyType>{EEnemyType.RED_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.RED_ENEMY}, new List<float>{0.5f, 0.3f, 0.3f, 0.5f}), new TimerCondition(60), new GetOilObjective(4)),
		new GameScenario(new NatureData(new List<int> {1,1,1,1}, new List<int>{1,10,20,25}, new List<EEnemyType>{EEnemyType.RED_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.RED_ENEMY}, new List<float>{0.5f, 0.3f, 0.3f, 0.5f}), new TimerCondition(60), new GetOilObjective(4)),
		new GameScenario(new NatureData(new List<int> {1,1,1,1}, new List<int>{1,10,20,25}, new List<EEnemyType>{EEnemyType.RED_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.RED_ENEMY}, new List<float>{0.5f, 0.3f, 0.3f, 0.5f}), new TimerCondition(60), new GetOilObjective(4)),
		new GameScenario(new NatureData(new List<int> {1,1,1,1}, new List<int>{1,10,20,25}, new List<EEnemyType>{EEnemyType.RED_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.RED_ENEMY}, new List<float>{0.5f, 0.3f, 0.3f, 0.5f}), new TimerCondition(60), new GetOilObjective(4)),

		new GameScenario(new NatureData(new List<int> {1,1,1,1}, new List<int>{1,10,20,25}, new List<EEnemyType>{EEnemyType.RED_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.RED_ENEMY}, new List<float>{0.5f, 0.3f, 0.3f, 0.5f}), new TimerCondition(60), new GetOilObjective(4)),
		new GameScenario(new NatureData(new List<int> {1,1,1,1}, new List<int>{1,10,20,25}, new List<EEnemyType>{EEnemyType.RED_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.RED_ENEMY}, new List<float>{0.5f, 0.3f, 0.3f, 0.5f}), new TimerCondition(60), new GetOilObjective(4)),
		new GameScenario(new NatureData(new List<int> {1,1,1,1}, new List<int>{1,10,20,25}, new List<EEnemyType>{EEnemyType.RED_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.RED_ENEMY}, new List<float>{0.5f, 0.3f, 0.3f, 0.5f}), new TimerCondition(60), new GetOilObjective(4)),
		new GameScenario(new NatureData(new List<int> {1,1,1,1}, new List<int>{1,10,20,25}, new List<EEnemyType>{EEnemyType.RED_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.RED_ENEMY}, new List<float>{0.5f, 0.3f, 0.3f, 0.5f}), new TimerCondition(60), new GetOilObjective(4)),
		new GameScenario(new NatureData(new List<int> {1,1,1,1}, new List<int>{1,10,20,25}, new List<EEnemyType>{EEnemyType.RED_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.BLACK_ENEMY, EEnemyType.RED_ENEMY}, new List<float>{0.5f, 0.3f, 0.3f, 0.5f}), new TimerCondition(60), new GetOilObjective(4)),
		*/
//	};

	int _scene =0;
	IGameScenario _currentScene;
	public IGameScenario Scene {get{return _currentScene;}}
	public int SceneNumber {get {return _scene;} }


	void Awake()
	{
		_scenes = new List<GameScenario> ();
		for (int i = 0; i < _levels.Count; i++) {
			_scenes.Add (new GameScenario(_levels [i]));
		}
	}

	void Start () 
	{
	//	SetScenario (_scenes [_scene]);

	}

	void Update () 
	{

	}

	public void SetScenario(IGameScenario scene)
	{
		if (_currentScene != null)
			_currentScene.Finish ();
		_nature.Clear ();
		scene.Start (_nature);

		_currentScene = scene;
	}

	public void SetScenario(int sceneNumber)
	{
		_scene = sceneNumber;
		_scenes [_scene] = new GameScenario (_levels [_scene]);
		if(_scene >= _scenes.Count)
		{
			_scene = _scene % _scenes.Count; 
		}

		SetScenario (_scenes [_scene]);
	}


	public void NextScene()
	{
		_scene++;
		if(_scene >= _scenes.Count)
		{
			_scene = _scene % _scenes.Count; 
		}

		SetScenario (_scenes [_scene]);
	}

	public void Back()
	{
		_scene = 0;
		SetScenario (_scenes [_scene]);
	}




}
