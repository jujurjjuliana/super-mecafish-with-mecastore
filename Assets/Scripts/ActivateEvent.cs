﻿using UnityEngine;
using System.Collections;

public class ActivateEvent : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ToggleActive(){
		if (!gameObject.activeSelf) {
			gameObject.SetActive (true);
		} else {
			gameObject.SetActive (false);
		}
	}

}
