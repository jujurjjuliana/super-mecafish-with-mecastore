﻿using UnityEngine;
using System.Collections;

public class RedEnemy : Enemy 
{
	public IRedEnemyEventBroadcasterTrigger eventTrigger = null;

	bool _isEnable = false;

	[SerializeField] GameObject sound;
	[SerializeField] Transform _centerRoute;
	[SerializeField] float _secondsTurn;

	Vector3 CenterRoute;

	void Awake(){
		base.Awake ();
		CenterRoute = _centerRoute.position;
		SetData (new RedEnemyData ());
	}

	void OnDrawGizmos(){
		if (!Application.isPlaying) {
			if (_centerRoute != null) {
				Gizmos.DrawWireSphere (_centerRoute.position, (_centerRoute.position - transform.position).magnitude);
			}
		} else {
			if (_centerRoute != null) {
				Gizmos.DrawWireSphere (CenterRoute, (CenterRoute - transform.position).magnitude);
			}
		}
	}

	void Update()
	{

		if (!base.Live)
		{
			return;
		} 
		//else base.UpdatePath ();
		circleMovement();


		/*if ((base.PosPath >= 0 && base.PosPath <= 8) || (base.PosPath >= 18 && base.PosPath <= 26)) 
		{
			gameObject.GetComponent<SpriteRenderer> ().enabled = false;
			_isEnable = false;
		}
		else
		{
			gameObject.GetComponent<SpriteRenderer> ().enabled = true;
			_isEnable = true;
		}*/


		if (base._nature.CheckFishContact (transform.position, EEnemyType.RED_ENEMY) && eventTrigger != null /*&& _isEnable*/) 
		{		
			//Essa implementação permite que o peixe aguente varios hits, é uma gambiarra implementada assim devido a dificuldade em manusear eventos
			//não documentados e dificeis de entender.
			Harbor.Instance._launched [0].TakeHit ();
			if (Harbor.Instance._launched [0].IsAlive ()) {
				return;
			}
			sound = GameObject.Find ("GameOverSound");

			if (sound != null) 
			{
				sound.GetComponent<AudioSource> ().Play ();
			}

			eventTrigger.TriggerFishContact (this);
		}
	}

	void circleMovement(){

		transform.RotateAround (CenterRoute, Vector3.forward, Time.deltaTime * 360f / _secondsTurn);
		transform.rotation = Quaternion.identity;

	}


}


