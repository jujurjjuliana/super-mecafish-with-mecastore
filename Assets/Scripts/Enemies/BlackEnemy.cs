﻿using UnityEngine;
using System.Collections;

public class BlackEnemy : Enemy
{
	[SerializeField] GameObject brokenEnemy;

	bool _isEnable  = false;

	[SerializeField] GameObject sound;

	const float ENEMY_DISTANCE_TO_GET_BLACK_ENEMY = 1;
	const float ENEMY_SQRT_DISTANCE_TO_GET_BLACK_ENEMY = ENEMY_DISTANCE_TO_GET_BLACK_ENEMY * ENEMY_DISTANCE_TO_GET_BLACK_ENEMY;

	public IBlackEnemyEventBroadcasterTrigger eventTrigger = null;

	void Awake(){
		base.Awake ();
		SetData (new BlackEnemyData ());

	}

	void Update()
	{
		
		if (!base.Live)
		{
			return;
		} 
		//else base.UpdatePath ();


		/*if ((base.PosPath >= 0 && base.PosPath <= 8) || (base.PosPath >= 18 && base.PosPath <= 26)) 
		{
			gameObject.GetComponent<SpriteRenderer> ().enabled = true;
			_isEnable = true;
		}
		else
		{
			
			gameObject.GetComponent<SpriteRenderer> ().enabled = false;
			_isEnable = false;
		}*/


		if (base._nature.CheckFishContact (transform.position, EEnemyType.BLACK_ENEMY) && eventTrigger != null /*&& _isEnable*/) 
		{
			//registrar em evento
			//colocar a posição do enemy no data

			sound = GameObject.Find ("CollectSound");

			if (sound != null) 
			{
				sound.GetComponent<AudioSource> ().Play ();
			}

			eventTrigger.TriggerFishContact (this);


			base._nature.QtdOilRemoved += 25;
			base._nature.Remove (transform.position, EEnemyType.BLACK_ENEMY, Data, this.gameObject);
//			if (base._nature.CreateEnemy (transform.position, EEnemyType.GREEN_ENEMY, 0.3f)) 
//			{
//				
				
//			}
		}
	}

	float SqrtDistance2D( Vector3 posA, Vector3 posB )
	{
		var x = posA.x - posB.x;
		var y = posA.y - posB.y;
		return x * x + y * y;
	}

}
