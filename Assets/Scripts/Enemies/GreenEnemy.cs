﻿using UnityEngine;
using System.Collections;

public class GreenEnemy : Enemy
{	
	public IGreenEnemyEventBroadcasterTrigger eventTrigger = null;

	float delta;

	[SerializeField] GameObject sound;

	void Update ()
	{

		delta += 0.01f; 

		if (delta > 2) 
		{

			if (base._nature.CheckFishContact (transform.position, EEnemyType.GREEN_ENEMY) && eventTrigger != null)
			{			

				sound = GameObject.Find ("CollectSound");

				if (sound != null) 
				{
					sound.GetComponent<AudioSource> ().Play ();
				}

				eventTrigger.TriggerFishContact (this);
				/*verificar se o tanque está cheio antes de chamar o comando de remover. Só posso chamar
			a função Remove se tiver certeza que o tanque ficou cheio por conta deste greenEnemy*/
				//método Remove deve ser registrado para executar no evento TriggerFishContact?

				base._nature.Remove (transform.position, EEnemyType.GREEN_ENEMY, base.Data, this.gameObject);
				base._nature.QtdOilRemoved += 50;
			}
		}

	}


}
