﻿using UnityEngine;
using System.Collections;

public class BrokenEnemy : Enemy
{
	[SerializeField] GameObject greenEnemy;


	float delta;

	
	void Update () 
	{
		delta += 0.01f; 

		if (delta > 2) 
		{
			
			if(base._nature.CreateEnemy (transform.position, EEnemyType.GREEN_ENEMY, 0.3f))
				base._nature.Remove (transform.position, EEnemyType.BROKEN_ENEMY, Data, this.gameObject);
		}

	}
}
