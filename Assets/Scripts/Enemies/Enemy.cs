﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface IEnemy
{
	bool Live { get;}
	IEnemyData Data { get;}
}

public interface IEnemyEvents
{
	
}

public abstract partial class Enemy : MonoBehaviour, IEnemy
{
	#region Static
	static List<Enemy> _all = new List<Enemy>();
	public static IList<Enemy> All 
	{ 
		get 
		{ 
			for( int i = _all.Count - 1; i >= 0; i-- )
			{
				if( !IsValid( _all[i] ) )
					_all.RemoveAt( i );
			}

			return _all; 
		} 
	}
	#endregion Static

	[SerializeField] Transform _cachedTransform;
	[SerializeField] float MaximumSpeed;



	IEnemyData _data;
	public IEnemyData Data {get { return _data; } }


	public virtual void SetData(IEnemyData data)
	{
		_data = data;
	}


	//Positions in the path region
	List<Vector3> _path;
	public List<Vector3> Path {get { return _path;} }

	//Enemies'position in path region
	int _posPath;
	public int PosPath { get { return _posPath; } set { _posPath = value;} }

	bool _isAlive = true;
	public bool Live { get { return _isAlive; } }

	public GameRegions _regions;
	public OceanNature _nature;

	protected virtual void Awake()
	{
		_all.Add (this);
	}

	void Start()
	{

		_regions = Guaranteed<GameRegions>.Instance;
		_nature = Guaranteed<OceanNature>.Instance;

		_path = _regions.RoundRegion;
	}

	void Update()
	{
		if( !_isAlive ) 
		{
			return;
		}
		else UpdatePath ();
	}


	public static bool IsValid(Enemy enemy)
	{
		return enemy!=null && enemy.Live;
	}

	public void UpdatePath()
	{
		if (EnableInput.Pause) 
		{

			var _distance = MaximumSpeed * 0.15f;

			var initialPos = _cachedTransform.position;

			var dir = _path[_posPath] - initialPos;

			var endPos = initialPos + dir * _distance;

			_cachedTransform.position = endPos;

			Data.Position = endPos;

			var dist = Vector3.Distance (_path[_posPath], _cachedTransform.position);

			if (dist < 0.2f)
			{				
				++_posPath;
			}

			if (_posPath == 36)
				_posPath = 0;
		}


			
	}


	public void Kill()
	{
		if( this.gameObject != null )
		{			
			Destroy( this.gameObject );
		}
	}


}

class EnemyEvents: IEnemyEvents
{
	
}