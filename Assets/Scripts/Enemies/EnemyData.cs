﻿using UnityEngine;
using System.Collections;

public enum EEnemyType {BLACK_ENEMY, RED_ENEMY, GREEN_ENEMY, BROKEN_ENEMY}

public interface IEnemyData
{
	string Name {get;}
	int ID { get; }
	EEnemyType Type { get; }
	Vector3 Position { get; set;}
	void SetDataInfo (int number, Vector3 position);

}

public abstract class EnemyData: IEnemyData
{
	public abstract EEnemyType Type {get;}
	public abstract string Name {get;}
	public int ID { get; private set; }
	public Vector3 Position{ get; set;}

	public EnemyData(int number, Vector3 position, float speed)
	{
		this.ID = number;
		this.Position = position;
	}
	public EnemyData()
	{
		this.ID = 0;
		this.Position = Vector3.zero;
	}
	public void SetDataInfo(int number, Vector3 position)
	{
		this.ID = number;
		this.Position = position;
	}
}

public class BlackEnemyData :EnemyData
{
	public override EEnemyType Type { get { return EEnemyType.BLACK_ENEMY; } }
	public override string Name{ get { return "BlackEnemy";} }

	public BlackEnemyData(int number, Vector3 position, float speed) : base(number, position, speed){}	
	public BlackEnemyData () : base (){}
}

public class RedEnemyData :EnemyData
{
	public override EEnemyType Type { get { return EEnemyType.RED_ENEMY; } }
	public override string Name{ get { return "RedEnemy";} }

	public RedEnemyData(int number, Vector3 position, float speed) : base(number, position, speed){}	
	public RedEnemyData () : base (){}
}

public class GreenEnemyData :EnemyData
{
	public override EEnemyType Type { get { return EEnemyType.GREEN_ENEMY; } }
	public override string Name{ get { return "GreenEnemy";} }

	public GreenEnemyData(int number, Vector3 position, float speed) : base(number, position, speed){}	
	public GreenEnemyData () : base (){}
}

public class BrokenEnemyData :EnemyData
{
	public override EEnemyType Type { get { return EEnemyType.BROKEN_ENEMY;} }
	public override string Name{ get { return "Broken Enemy " + ID;} }

	public BrokenEnemyData(int number, Vector3 position, float speed) : base(number, position, speed){}	
}

