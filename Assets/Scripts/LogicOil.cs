﻿using UnityEngine;
using System.Collections;

public interface ILogicOil
{
	//void GetOil();
	Vector3 Position{ get; }

	float Rotation { get; }
	float Scale { get; }
	void Simulate( float time );
}

public class LogicOil: MonoBehaviour, ILogicOil
{
	public const float SECONDS_TO_INCREASE = 240;
	public const float MAX_SIZE = .3f;
	public const float SIZE_VARIATION = .05f;
	public const float SEPARATION = .5f;
	public const float PLANE_DISTANCE = .2f;
	
	public static float MINIMUM_SPARATION = CalculateSeparation( MAX_SIZE - SIZE_VARIATION );
	
	Vector3 _pos;
	float _angle;
	float _seed;
	float _baseSize;
	bool _isAlive = true;
	float _lifeTime = 0;

	GameObject _go;
	[SerializeField] OceanNature _ocean;
	[SerializeField] GameRegions _regions;
	[SerializeField] int number;

	public int Number { get; private set; }

	bool _used = false;

	public LogicOil(int num)
	{
		number = num;
	}

	public void Simulate(float time)
	{
		_lifeTime += time;
		//		Rotation = Time.time;
		//		Scale = Mathf.Cos( Time.time ) + 1;
	}

	public GameObject Go{get{return _go;}}

	public Vector3 Position { get { return _pos; } }
	public float Rotation { get { return _angle; } }
	public float Scale { get { return _baseSize; } }
	public float Size{get{return _baseSize;} set{_baseSize = Size;}}

	public bool IsAlive { get { return _isAlive; } }
	
	public float Separation { get { return CalculateSeparation( _baseSize ); } }
		
	public void Kill()
	{ 
		_isAlive = false;
		if( OnDeath != null )
			OnDeath();
	}
	
	public System.Action OnDeath { private get; set; }
	
	public static float CalculateBaseSize( float range01 ) { return MAX_SIZE - ( range01 * SIZE_VARIATION ); }
	public static float CalculateSeparation( float size ) { return SEPARATION * size / MAX_SIZE; }

	public IOilEventBroadcasterTrigger eventTrigger = null;

	const float OIL_DISTANCE_TO_GET_OIL = 1;
	const float OIL_SQRT_DISTANCE_TO_GET_OIL = OIL_DISTANCE_TO_GET_OIL * OIL_DISTANCE_TO_GET_OIL;

	void Start()
	{
		_ocean = Guaranteed<OceanNature>.Instance;
		_regions = Guaranteed<GameRegions>.Instance;

		_pos = transform.position;
		_seed = Random.value;
		_angle = _seed % ( Mathf.PI * 2 );
		_go = this.gameObject;
	}

	void Update()
	{
		CheckFish ();
	}

	public void GetOil()
	{
		eventTrigger.TriggerGetOil (this);
	}

	public void CheckFish()
	{
		var _fish = Fish.All;

		if (_regions.BelowFish (transform.position)) 
		{
			for (int i = 0; i < _fish.Count; ++i)
			{
				var sqrtDist = SqrtDistance2D(transform.position, _fish[i].transform.position);

				if (sqrtDist < OIL_SQRT_DISTANCE_TO_GET_OIL && _fish[i].Full) 
				{
					_ocean.delete (this.Position);
				}
			}

		
		}
	}

	float SqrtDistance2D( Vector3 posA, Vector3 posB )
	{
		var x = posA.x - posB.x;
		var y = posA.y - posB.y;
		return x * x + y * y;
	}


}
