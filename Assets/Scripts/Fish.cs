﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;

public interface IFish
{
	bool Live {	get; }
	void Unregister();

	void EnqueeLanding();
	void EnqueeDestination( Vector2 vector2 );
	void ClearOrders();
	void SetPathSkin( LineRendererSkinRule _selectedPath );
	void TakeHit ();
	bool IsAlive ();
	float GetCoinRateBonus();
	void Launch();

	IFishData Data { get; }

	//*FishTank FishTank { get; }
	float MaximumSpeed { get; }
	//*IFishEvents Events { get; }
	void Kill();

	bool canMove { get; }


	bool Launched { get; }
	void LandedData( System.Func<Vector3> posProvider, System.Func<Quaternion> rotationProvider );
}

public interface IFishEvents
{
	IEventRegister<float> OnUpdate { get; }
	IEventRegister OnTankRelease { get; }
	IEventRegister<float> OnAfterUpdate { get; }
	IEventRegister OnDestroy { get; }
}

	public abstract partial class Fish : MonoBehaviour, IFish
{
	#region Static
	static List<Fish> _all = new List<Fish>();
	public static IList<Fish> All 
	{ 
		get 
		{ 
			for( int i = _all.Count - 1; i >= 0; i-- )
			{
				if( !IsValid( _all[i] ) )
					_all.RemoveAt( i );
			}

			return _all; 
		} 
	}
	#endregion Static

	[SerializeField] FloatAnimator _turnController;
	[SerializeField] FloatAnimator _altitudeController;

	[SerializeField] LineRenderer _pathRenderer;
	[SerializeField] LineRendererBlend _pathBlend;
	[SerializeField] float _touchRadius;
	[SerializeField] Transform fishSprite;
	[SerializeField] float _depositWaitTime;
	[SerializeField] int lives = 1;
	[SerializeField] ParticleSystem hitEffect;

	[SerializeField] GameObject[] _items;

	[SerializeField] float _litterCapacity = 1;
	float _litterQuantity;
	bool hitRecovering;
	float coinRatebonus = 0;

	[SerializeField] GameObject sound;

	public bool launched = false;
	bool _isAlive = true;
	bool _wasInOceanArea = true;
	[SerializeField] float _speed = 3.0f;

	Transform _cachedTransform;
	GameRegions _regions;
	OceanNature _ocean;

	bool _move = true;
	bool _depositing = false;
	bool _inDepot = false;

	public Transform posCauda;
	public Transform posHead;
	public Vector2 _head;

	public float MaximumSpeed { get {return _speed;}  }
	public bool canMove { get { return _move; } }
	public bool IsDepositing { get { return _depositing; } }
	public bool Live { get { return _isAlive; } }
	public bool Full{ get { return _litterQuantity >= _litterCapacity; } }
	public bool HasLitter { get { return _litterQuantity > 0; } }
	EventSlot<int> _intLevelChanged = new EventSlot<int>();
	public int IntLevel{ get { return Mathf.FloorToInt (_litterQuantity); } }
	public IEventRegister<int> OnIntLevelChanged { get { return _intLevelChanged; } }

	public Vector3 Position{get{ return _cachedTransform.position; }}

	System.Func<Vector3> _landPosProvider;
	System.Func<Quaternion> _landRotationProvider;

	System.Func<Vector3> _liftPosProvider;

	public void LandedData( System.Func<Vector3> posProvider, System.Func<Quaternion> rotationProvider )
	{
		_landPosProvider = posProvider;
		_landRotationProvider = rotationProvider;
	}


	public void LiftData( System.Func<Vector3> posProvider )
	{
		_liftPosProvider = posProvider;
	}

	public void ClearLiftData()
	{
		_liftPosProvider = null;
	}

	public static bool IsValid( Fish fish )
	{
		return fish!=null && fish._isAlive;
	}

	public float TouchRadius { get { return _touchRadius; } }


	public bool CanGatherOil { get; private set; }

	[SerializeField]
	public bool HasPath { get; private set; }

	public bool LandOnPathEnd { get; private set; }

	List< Vector2 > _destinations = new List<Vector2>();

	[SerializeField] bool _launched = false;
	public bool Launched { get { return _launched; } }

	IFishData _data; //= new SpeedFishData (0);
	public IFishData Data {get { return _data; } }

	public virtual void SetData( IFishData data )
	{
		_data = data;
	}

	FloatAnimator _positioningSpeed = new FloatAnimator( 0, 1000, 100, 200, 500 );



	//voltando depois de esvaziar o tanque
	/*public bool back = false;
	public bool Test { get { return test; } }
	public bool Test2 { get { return test2; } }
	public bool test = false;
	public bool test2 = false;
	public bool test3 = false;*/


	EventSlot<Vector3> _blackEnemyContact = new EventSlot<Vector3>();
	public IEventRegister<Vector3> OnBlackEnemy { get { return _blackEnemyContact; } }

	IEventTrigger<IEnemy> _event;

	public IFishEnemyEventBroadcasterTrigger eventTriggerTeste = null;



	protected virtual void Awake()
	{	

		sound = GameObject.Find ("ImpactSound");



		_ocean = Guaranteed<OceanNature>.Instance;

		_event = new DelegateEventListener<IEnemy>(CheckOilGather);

		_ocean.Events.GreenEvent.FishContact.Register (_event);
		_ocean.Events.BlackEvent.FishContact.Register (_event);


		CanGatherOil = true;
		HasPath = true;
		_all.Add( this );

		_cachedTransform = transform;


		//_cachedTransform.localScale = new Vector3 (0.27f, 0.25f, 0.25f);

		OnIntLevelChanged.Register (CheckTankTextureFull);

	}

	protected virtual void Start() 
	{
		//Debug.Log ("FID " + Fish.All.Count);

		_regions = Guaranteed<GameRegions>.Instance;

		eventTriggerTeste = (IFishEnemyEventBroadcasterTrigger)_ocean.Events.FishEvent;

	}


	public void Unregister()
	{
		OnIntLevelChanged.Unregister (CheckTankTextureFull);

	}


	void CheckTankTextureFull(int level)
	{
		if (level == 1) {
			fishSprite.gameObject.GetComponent<Animator> ().SetBool ("Full", true);
		} else if (level == 2) {
			fishSprite.gameObject.GetComponent<Animator> ().SetBool ("Full", true);
		}
	}

	void CheckTankTextureEmpty(int level)
	{
		if (level <= 0) {
			fishSprite.gameObject.GetComponent<Animator> ().SetBool ("Full", false);
		}
	
	}

	public void EnqueeLanding()
	{
		LandOnPathEnd = true;
	}

	public void EnqueeDestination( Vector2 position )
	{
		if(!HasPath) return;

		if( _destinations.Count > 0 )
		{
			var last = _destinations[ _destinations.Count - 1 ];
			var dist = Vector2.Distance( position, last );
			if( dist < .4f )
				return;
		}

		_destinations.Add( position );		
	}

	public void ClearOrders()
	{	
		_destinations.Clear();
	}

	public void TakeHit(){
		if (!hitRecovering) {
			lives -= 1;
			if (IsAlive()) {
				MasterAudio.PlaySound ("FishHit");
				StartCoroutine ("RecoverFromHit");
				hitEffect.Play ();
			}

		}

	}
	public bool IsAlive(){
		return lives > 0;
	}

	public void SetPathSkin( LineRendererSkinRule skinRule )
	{
		//_pathBlend.SetNewRule( skinRule );
	}


	//Rotate the fish to look to the next waypoint in the array
	public void Rotate(){
		if( !HasPath || _destinations.Count == 0 )
		{
			return;
		}

		var destination = _destinations[0];
		Vector3 target = new Vector3 (destination.x, destination.y);
		//Define two vectors
		Vector3 v1 =   _cachedTransform.right;
		Vector3 v2 = target - _cachedTransform.position;
		//Projects into xy plane
		v1.z = 0;
		v2.z = 0;
		//gets the angle between them
		//Angle = Arccosine[(<v1,v2>)/(|v1|*|v2|)]
		float cosAngle = Mathf.Clamp(Vector3.Dot(v1,v2)/(v1.magnitude*v2.magnitude),0,1);
		float angleInRadians = Mathf.Acos(cosAngle);
		//Convert from radians to degree
		float angleInDegrees = (180 * angleInRadians) / Mathf.PI;
		//determine if rotate up or down
		int direction = 1;

		//Use the line equation to determine the direction of the rotation
		//Point 1 (x1,y1) is the fish
		float x1 = _cachedTransform.position.x;
		float y1 = _cachedTransform.position.y;
		//Point 2 (x2,y2) is a point in front of the fish 
		float x2 = v1.x + x1;
		float y2 = v1.y + y1;
		//We get the x component of the target and apply to the line equation to determine the y component.
		//The y component will be used to determine if the rotation is postive or negative
		float x = target.x;
		if (x1 != x2) { //If x1 = x2, then we use x to determine
			float y = ((y1 - y2) / (x1 - x2)) * x + (x1 * y2 - x2 * y1) / (x1 - x2);
			if (target.y > y) {
				direction = 1;
			} else {
				direction = -1;
			}
		} else {
			if (x < _cachedTransform.position.x) {
				direction = 1;
			} else {
				direction = -1;
			}
		}
		//The logic is inverted if the Fish is facing left
		if (_cachedTransform.rotation.eulerAngles.z > 90 && _cachedTransform.rotation.eulerAngles.z< 270) {
			direction = -direction;
		}


		//Rotates the sprite dependending if the fish is facing left or right
		if (_cachedTransform.rotation.eulerAngles.z > 90 && _cachedTransform.rotation.eulerAngles.z < 270) {

			var myRot = Quaternion.RotateTowards( fishSprite.localRotation, Quaternion.Euler(Vector3.right * 180), 200 );
			fishSprite.localRotation = myRot;

		} else if ((_cachedTransform.rotation.eulerAngles.z < 90 || _cachedTransform.rotation.eulerAngles.z > 270)) {

			var myRot = Quaternion.RotateTowards( fishSprite.localRotation, Quaternion.identity, 200 );
			fishSprite.localRotation = myRot;
		}

		//Rotate around the Z axis
		Vector3 rotEuler = _cachedTransform.rotation.eulerAngles + Vector3.forward * angleInDegrees*direction;
		Quaternion rotQuaternion = Quaternion.Euler (rotEuler);
		var newRot = Quaternion.RotateTowards( _cachedTransform.rotation,rotQuaternion, 1080 * GameTime.Delta );
		_cachedTransform.rotation = newRot;


	}






	void UpdateLaunchedFish( float delta )
	{		
		
		checkStorageCollision ();

		checkFishPosition ();

		var validRegion = _regions.OceanSpace;

		var initialPos = _cachedTransform.position;

		var pos2d = new Vector2( initialPos.x, initialPos.y );

		bool now = validRegion.Contains( pos2d );

		if( _wasInOceanArea && !now )
		{
			TurnBack();
		}

		_wasInOceanArea = now;

		//AutoPilot( delta );
		Rotate();

		var distance = MaximumSpeed * delta;
		if (!launched)
			distance = 0;
		if(canMove) Move( _cachedTransform.right, distance );

		_pathBlend.Update( _pathRenderer, delta );
		UpdatePathRenderer();

		//esvaziando o tanque
		if (_inDepot && !IsDepositing && HasLitter) 
		{
			Release ();
			if (!HasLitter) {
				fishSprite.gameObject.GetComponent<Animator> ().SetBool ("Full", false);
			}
					
		}

		if(_inDepot && !IsDepositing && !HasLitter)
		{		
			_destinations.Clear ();
//			fishSprite.gameObject.GetComponent<Animator> ().SetBool ("Stopped", false);
			TurnBack ();
			_inDepot = false;
			_move = true;
		}	

	}


	void Update() 
	{		
		var delta = GameTime.Delta;

		//CheckOilGather ();

		if( !_isAlive ) 
		{
			return;
		}
		else 
		{
			if (_launched)
				UpdateLaunchedFish (delta);
			else
			{
				UpdateLandedFish (delta);
			}
		}

	}

	//registrar este evento no GreenEvent  e eliminar  a necessidade de checar OverEnemy
	public void CheckOilGather(IEnemy enemy)
	{
		//var pos = _cachedTransform.position;

		//pra ele encher o tank aos poucos preciso saber o pos do enemy e só deixar ele pegar o óleo se o enemy atual está em
		//uma posição diferente da anterior
		if(CanGatherOil && !Full /*&& _regions.OverEnemy(pos)*/)
		{			
			
			Fill (enemy/*Time.deltaTime*/);

			//if(_fishTank.Full) _ocean._semaphore = 1;
		}
	}

	void UpdateLandedFish( float delta )
	{
		Vector3 pos = Vector3.zero;
		if( _liftPosProvider != null )
			pos = _liftPosProvider();
		else if( _landPosProvider != null )
			pos = _landPosProvider();

		AdjustPosition( pos, Time.unscaledDeltaTime );

	
	}

	void AdjustPosition( Vector3 position, float delta )
	{
		var fishPos = transform.position;
		var travel = position - fishPos;
		var dist = travel.magnitude;
		var tn = travel / dist;

		if( dist > .0001f )
		{
			_positioningSpeed.SetDesire( dist / delta );
			_positioningSpeed.Update( delta );

			var speed = Mathf.Min( _positioningSpeed.Value, dist / delta );
			_positioningSpeed.SetDesire( speed );
			fishPos += tn * speed * delta;
			transform.position = fishPos;
		}
		else
		{
			_positioningSpeed.SetDesire( 0 );
			_positioningSpeed.Update( delta );
		}
	}


	public void Kill()
	{

		if( this.gameObject != null )
		{			
			Destroy( this.gameObject );
		}
	}

	public void Launch()
	{
		fishSprite.gameObject.GetComponent<Animator> ().SetBool ("Full", false);
		TurnBack ();
		_launched = true;
		ApplyItem ();

	}


	void UpdatePathRenderer()
	{
		if( _pathRenderer == null )
			return;

		var active = _destinations.Count > 0;
		_pathRenderer.gameObject.SetActive( active );

		if( !active )
			return;
		_pathRenderer.SetVertexCount(_destinations.Count + 1 );

		_pathRenderer.SetPosition( 0, transform.position );

		for( int i = 0; i < _destinations.Count; i++ )
		{
			var dest = _destinations[i];
			var pos = ToWorld( dest ) + Constants.Measures.FISH_PATH_DESLOCATION;

			_pathRenderer.SetPosition( i + 1, pos );
		}
	}

	void Move( Vector3 step ) { Move( step.normalized, step.magnitude ); }

	void Move( Vector3 dir, float distance )
	{
		var initialPos = _cachedTransform.position;
		var endPos = initialPos + dir * distance;

		_cachedTransform.position = endPos;		

		while( _destinations.Count > 0 )
		{	
			var destination = ToWorld( _destinations[0], initialPos.z );

			var minDist = Algorithm.DistancePointLine( destination, initialPos, endPos );

			//nao esta perto o suficiente para ser removido da lista _destinations
			if( minDist > 0.2f/*DestinationPrecision*/ )
			{	
				//Debug.Log ("DestinationPrecision = " + DestinationPrecision);
				break;
			}

			_destinations.RemoveAt(0);


			if( _destinations.Count == 0 && LandOnPathEnd )
			{
				//Land();
			}
		}
	}


	void Land()
	{
		_launched = false;
		Harbor.Instance.AddToLaunchLine( this );

		Debug.Log ("land");
	}


	void TurnBack()
	{
		transform.Rotate (Vector3.forward * 180);
		transform.Rotate (Vector3.right * 180);

	}

	Vector3 ToWorld( Vector2 pos )
	{
		return new Vector3( pos.x,  pos.y, _cachedTransform.position.z);
	}

	Vector3 ToWorld( Vector2 pos, float z )
	{
		return new Vector3( pos.x, pos.y,z );
	}


	//determina se o Fish colide com o Storage, está dentro do Storage ou na entrada, esvazia o tanque se estiver dentro
	public void checkFishPosition()
	{

		var oilStorageArea = _regions.OilStorageArea;

		var _posHead = new Vector2 (posHead.position.x, posHead.position.y);

		if (oilStorageArea.Contains (_posHead) && HasLitter) {
			//_cachedTransform.eulerAngles = new Vector3 (0, 116.4f, 0);
			_move = false;
			_inDepot = true;
//			fishSprite.gameObject.GetComponent<Animator> ().SetBool ("Stopped", true);

			_destinations.Clear ();
		}
		else if (oilStorageArea.Contains (_posHead) && !HasLitter)
		{
//			fishSprite.gameObject.GetComponent<Animator> ().SetBool ("Stopped", false);
			_destinations.Clear ();
		}
	}

	void checkStorageCollision()
	{
		//verifica se o peixe está colidindo com o Storage
		var p = posHead.position;
		_head = new Vector2( p.x, p.y );

		var MachineRegions = _regions.MachineRegions;
		for (int index = 0; index < MachineRegions.Length; index++) {
			if (MachineRegions [index].Contains(_head)) {
				if (sound != null) 
				{
					sound.GetComponent<AudioSource> ().Play ();
				}
				_destinations.Clear ();
				TurnBack ();
			}

		}
	}


	IEnemy _enemy;

	float Level
	{
		get
		{
			return _litterQuantity;
		}
		set
		{
			int initialValue = IntLevel;

			_litterQuantity = value;
			if (_litterQuantity >= _litterCapacity) _litterQuantity = _litterCapacity;
			if (_litterQuantity < 0) _litterQuantity = 0;

			int finalValue = IntLevel;

			if (finalValue != initialValue) 
			{

				if (_enemy.Data.Name == "BlackEnemy")
					_intLevelChanged.Trigger (1);
				else
					_intLevelChanged.Trigger (2);
				//de acordo com o valor do finalValue eu mudo a textura(0 tankvazio 1 tankcheio)
			}

		}
	}



	public void Fill(IEnemy enemy)
	{
		if (Full)
			return;
		_enemy = enemy;
		Level++;

	}

	public void Release()
	{
		GameStateController.Instance.MachineReciclingActivate ();
		Level -= 1;//seconds / _timeToFill;

		_depositing = true;
		StartCoroutine ("WaitingDeposit");
	}

	IEnumerator WaitingDeposit(){
		yield return new WaitForSeconds (_depositWaitTime);
		_depositing = false;
	}


	void Clamp()
	{
		if (_litterQuantity > _litterCapacity)
			_litterQuantity = _litterCapacity;
	}

	public void FillLevel(int level)
	{
		_litterQuantity += level;
		Clamp ();
	}



	public void ReleaseAll()
	{
		_litterQuantity = 0;
	}

	//Esta região contem funçoes relativas aos items equipados. Esta implementaçao precisa ser melhor discutida
	#region ItemFunctions

	//Função temporária que aplica o efeito dos itens. Esta implementaçao deve ser generalizada no futuro.
	public void ApplyItem(){
		int selectedSkin = GameSave.GetSelectedSkin ();
		if (selectedSkin >= 0) {
			_items [selectedSkin].SetActive (true);
		}
		switch (selectedSkin) 
		{
			case 0:
				_speed += 0.8f;
				break;
			case 1:
				coinRatebonus += .2f;
				break;
			case 2:
				lives += 1;
				break;
			case 3:
				_litterCapacity++;
					break;
			case 4:
				_speed += 0.8f;
				break;
			case 5:
				GameStateController.Instance.Director.Scene.FailCondition.AddValue (5);
				break;

			
		}
		
	}

	public float GetCoinRateBonus(){
		return coinRatebonus;
	}

	IEnumerator RecoverFromHit(){
		hitRecovering = true;
		fishSprite.gameObject.GetComponent<Animator> ().SetTrigger ("Hit");
		yield return new WaitForSeconds (1.6f);
		hitRecovering = false;
	}

	#endregion

}


public class FishEvents : IFishEvents
{
	EventSlot<float> _update = new EventSlot<float>();
	EventSlot<float> _afterUpdate = new EventSlot<float>();
	EventSlot _tankRelease = new EventSlot();
	EventSlot _destroy = new EventSlot();

	public IEventRegister<float> OnUpdate { get { return _update; } }
	public IEventRegister OnTankRelease { get { return _tankRelease; } }
	public IEventRegister<float> OnAfterUpdate { get { return _afterUpdate; } }
	public IEventRegister OnDestroy { get { return _destroy; } }

	public IEventTrigger<float> Update { get { return _update; } }
	public IEventTrigger TankRelease { get { return _tankRelease; } }
	public IEventTrigger<float> AfterUpdate { get { return _afterUpdate; } }
	public IEventTrigger Destroy { get { return _destroy; } }
}