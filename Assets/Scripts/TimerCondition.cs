﻿using UnityEngine;
using System.Collections;

public class TimerCondition  : ICondition
{
	float createdTime;
	float seconds;
	int maxValue;

	public float Value{get{return (Time.time - createdTime);}}
	public float MaxValue{get{return (maxValue);}}


	IEventTrigger<IEnemy> _event;
	public bool _isContact = false;

	OceanNature _nature;


	public TimerCondition (int qtdSeconds)
	{
		seconds = qtdSeconds;
		maxValue = qtdSeconds;
	}

	public void AddValue (int value){
		seconds += value;
		maxValue += value;
	}

	public void Init()
	{
		createdTime = Time.time;

		_nature = Guaranteed<OceanNature>.Instance;

		_nature.Events.RedEvent.FishContact.Register (_event);
	}


	public bool IsValid
	{
		get
		{
			if ((Time.time - createdTime < seconds) && !_isContact) 
			{				
				return true;
			}
			else
			{
				return false;
			}
		}
	}


	public void RedContact(IEnemy enemy)
	{		
		_isContact = true;
	}

	public void BeforeRegister()
	{
		_event = new DelegateEventListener<IEnemy> (RedContact);
		_isContact = false;
	}

	public void Unregister()
	{
		_event = null;
	}

}
