﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;

public interface IGameStateController 
{
	IGameScenarioControl Director {get;}
	IGameNature Nature {get;}

	UIController Ui { get;}

	void GoToGame ();
	void GoToScoreboard();
	void GoToPause ();
	void GoToSplash();
	void Back ();
	void GoToGameOver();
}

public class GameStateController  : MonoBehaviour, IGameStateController  
{
	[SerializeField] OceanNature  nature;
	[SerializeField] GameScenarioControl   director;
	[SerializeField] CameraController _camera;
	[SerializeField] UIController  _ui;
	[SerializeField] Animator MachineAnimator;
	[SerializeField] float CoindRateBonus;

	public static GameStateController _instance;

	public static GameStateController Instance{
		get{
			return _instance;
		}
	}

	public string state;

	public IGameScenarioControl Director{get{return director;}}
	public UIController Ui {get {return _ui;} }
	public IGameNature Nature { get{return  nature;} }

	public void GoToGame() {SetState (States.Game);}
	public void GoToScoreboard() {SetState (States.Scoreboard);}
	public void GoToPause () {SetState (States.Pause);}
	public void GoToSplash() {SetState (States.Splash);}
	public void GoToGameOver() {SetState (States.GameOver);}

	public void Back()
	{
		stateSequence.RemoveLast ();
		var state =  stateSequence.Last.Value;
		stateSequence.RemoveLast ();
		SetState (state);
	}

	public static class States
	{
		static readonly GameState   game = new GameState ();
		static readonly SplashState   splash = new SplashState ();
		static readonly PauseState   pause = new PauseState  ();
		static readonly ScoreboardState   scoreboard = new ScoreboardState ();
		static readonly GameOver gameOver = new GameOver ();


		public static IGameState  Game { get{ return  game; } }
		public static IGameState  Splash { get{return  splash; } }
		public static IGameState  Pause { get {return  pause;} }
		public static IGameState  Scoreboard {get {return  scoreboard;} }
		public static IGameState  GameOver { get{ return  gameOver; } }
	}

	LinkedList<IGameState >  stateSequence = new LinkedList<IGameState >();
	IGameState currentState;
	Harbor harbor;

	void Awake()
	{
		_instance = this;
		harbor = Guaranteed<Harbor>.Instance;
		GameSave.load ();
	}

	void Start () 
	{
		SetInitialState (States.Splash);
		_ui.SetState (States.Splash);
	}

	void Update () 
	{
		if( currentState != null)
		{
			 currentState.Update(this, Time.unscaledDeltaTime);
		}

		state =  currentState.ToString ();
	}

	public void MachineReciclingActivate(){
		MachineAnimator.SetTrigger ("Recicling");
		MasterAudio.PlaySound ("Recicling");

	}
	public void MachineReciclingDeactivate(){
		//MachineAnimator.SetBool ("Recicling",false);
	}


	IMachineRecicleEventBroadcasterTrigger machineEventTrigger;
	public void MachineRecicleComplete(){
		if (machineEventTrigger == null) {
			machineEventTrigger = (IMachineRecicleEventBroadcasterTrigger)nature.Events.MachineEvent;
		}
		machineEventTrigger.TriggerMachineRecicle (1);
		nature.QtdOilRemoved += 200;
	}

	public void SetState(IGameState  state)
	{
		if ( (state != null && state ==  currentState) || (state==null &&  currentState==null) )
			return;
		
		ClearState ( currentState);

		stateSequence.AddLast (state);
		currentState = state;

		if (state == null)
			return;

		_ui.ClearStates ();

		state.Enter (this);
		//StartCoroutine( _camera.CameraTransition( state ) );

		_ui.SetState (state);
	}

	public void SetInitialState(IGameState  state)
	{
		 stateSequence.AddLast (state);
		 currentState = state;
		 
		_ui.ClearStates ();

		state.Enter (this);
		_camera.Set( state.CameraState );

		_ui.SetState (state);

	}



	public void ClearState(IGameState  state)
	{
		if ( currentState == null)
			return;

		state.Exit (this);
	}

	public void SkipScoreBoard() 
	{		
		
	}

	public void Pause()
	{
		

		if (EnableInput.Pause) 
		{
			Time.timeScale = 0;
			EnableInput.Pause = false;
			Ui.showPausePanel ();
		}
		else
		{
			EnableInput.Pause = true;
			Time.timeScale = 1;	
			Ui.hidePausePanel ();
		}

	}
	public void repeatStage(){
		SelectStage (director.SceneNumber);

	}
	public void SelectStage(int stageNumber){
		if (stageNumber == 0) {
			if (!GameSave.GetTutorial()) {
				Ui.ShowTutorial ();
				return;
			}
		}
		director.SetScenario (stageNumber);
		GoToGame ();
	}

	//Workaround for the return to menu button in pause menu;
	public void ResetFishes(){
		Harbor.Instance.ResetFishes ();
	}

}