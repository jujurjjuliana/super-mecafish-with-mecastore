﻿using UnityEngine;
using System.Collections;

public interface ICondition 
{
	bool IsValid { get; }
	void Init();
	float Value{ get; }
	float MaxValue{ get; }
	void Unregister();
	void BeforeRegister();
	void AddValue (int value);

}
