﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface ISemaphore
{
	void Block( object obj );
	void BlockButDoNotIncrease( object obj );
	void Release( object obj );
	bool Free { get; }
}

public interface IReverseSemaphore
{
	void Request( object obj );
	void RequestButDoNotIncrease( object obj );
	void RemoveRequest( object obj );
	bool Requested { get; }
}

public class ReverseSemaphore : IReverseSemaphore
{
	Semaphore _semaphore = new Semaphore();

	public void Request( object obj ) { _semaphore.Block( obj ); }
	public void RequestButDoNotIncrease( object obj ) { _semaphore.BlockButDoNotIncrease( obj ); }
	public void RemoveRequest( object obj ) { _semaphore.Release( obj ); }
	public bool Requested { get { return !_semaphore.Free; } }
}

public class Semaphore : ISemaphore
{
	class SemaphoreObject
	{
		public int Value { get; set; }
	}
	
	Dictionary<object,SemaphoreObject> _semaphores = new Dictionary<object,SemaphoreObject>();
	public bool Free { get { return _semaphores.Count == 0; } }
	
	public void Block( object obj )
	{
		if( obj == null )
			return;
		
		SemaphoreObject s;
		if( !_semaphores.TryGetValue( obj, out s ) )
		{
			s = new SemaphoreObject();
			_semaphores.Add( obj, s );
		}
		
		s.Value++;
	}
	
	public void BlockButDoNotIncrease( object obj )
	{
		if( obj == null )
			return;
		
		SemaphoreObject s;
		/*if( !_semaphores.TryGetValue( obj, out s ) )
		{
			s = new SemaphoreObject();
			_semaphores.Add( obj, s );
			s.Value++;
		}*/

		if (!_semaphores.ContainsKey (obj))
		{
			s = new SemaphoreObject();
			_semaphores.Add (obj,s);
			s.Value++;
		}
	}
	
	public void Release( object obj )
	{
		if( obj == null )
			return;
		
		
		SemaphoreObject s;
		if( _semaphores.TryGetValue( obj, out s ) )
		{
			s.Value--;
			if( s.Value == 0 ) 
				_semaphores.Remove( obj );
		}
	}
	
	void Clear()
	{
		_semaphores.Clear();
	}
}