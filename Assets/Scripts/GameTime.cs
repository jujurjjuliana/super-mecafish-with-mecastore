using UnityEngine;

public class GameTime : MonoBehaviour
{
	static FloatAnimator _scaleAnimator = new FloatAnimator( 0, 1, .2f, .2f, 1 );
	static float _scale = 1;
	public static float Scale { get { return _scale; } set { _scaleAnimator.SetDesire( value ); } }
	public static float Delta { get { return Time.deltaTime * _scale; } }
	
	static float _total = 0;
	static float _timerTotal = 0;
	public static float Total { get { return _total; } }
	public static float TimerTotal { get { return _timerTotal; } }

	void Start()
	{
		_scaleAnimator.Start( 0 );
	}
	
	void Update()
	{
		_timerTotal += Time.deltaTime * _scaleAnimator.DesiredValue;
		_total += Delta;
		_scaleAnimator.Update( Time.unscaledDeltaTime );
		Time.timeScale = _scaleAnimator.Value;
	}
}
