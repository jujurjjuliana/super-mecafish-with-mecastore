﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIController : MonoBehaviour 
{
	[SerializeField] GameObject Splash;
	[SerializeField] GameObject ScoreBoard;
	[SerializeField] GameObject Pause;
	[SerializeField] GameObject Game;
	[SerializeField] GameObject GameOver;
	[SerializeField] GameObject LevelsPanel;
	[SerializeField] GameObject NextPageBtn;
	[SerializeField] GameObject PreviousPageBtn;
	[SerializeField] GameObject StorePanel;
	[SerializeField] GameObject Tutorial;
	[SerializeField] GameObject FacebookButton;
	public GameObject SyncingScreen;


	[SerializeField] GameObject CoinsObject;
	[SerializeField] StagePanel[] StagesPanels;


	[SerializeField] GameObject MenuSettings;
	[SerializeField] GameObject FacebookMenuBtnBlock;


	[SerializeField] Text _stopwatch;
	[SerializeField] Text _points;
	[SerializeField] Text _scoreboardpts;
	[SerializeField] Text _gameOverpts;
	[SerializeField] Text _stage;
	[SerializeField] Text _stageGameOver;
	[SerializeField] Text _coins;
	[SerializeField] Text _coinsStore;

	[SerializeField] Text _stageScoreBoard;
	[SerializeField] Text _stageScoreBoardCoins;
	[SerializeField] Text _stageScoreBoardBonusCoins;

	[SerializeField] GameObject _scoreBoardBonusGroup;

	[SerializeField] StoreUiController _storeUiController;


	public string Timer{get {return _stopwatch.text;}  set { _stopwatch.text = value;} }
	public string Points{get {return _points.text;}  set { _points.text = value;} }

	public string ScoreBoardpts { get { return _scoreboardpts.text; } set { _scoreboardpts.text = value; } }
	public string ScoreBoardCoins { get { return _stageScoreBoardCoins.text; } set { _stageScoreBoardCoins.text = value; } }
	public string ScoreBoardBonusCoins { get { return _stageScoreBoardBonusCoins.text; } set { _stageScoreBoardBonusCoins.text = value; } }
	public GameObject ScoreBoardBonusGroup { get { return _scoreBoardBonusGroup; } set { _scoreBoardBonusGroup = value; } }

	public string GameOverPts {get { return _gameOverpts.text; } set{ _gameOverpts.text = value; }}

	public string Stage {get { return _stage.text; } set{ _stage.text = value; }}

	public string StageGameOver {get { return _stageGameOver.text; } set{ _stageGameOver.text = value; }}

	public string StageScoreBoard {get { return _stageScoreBoard.text; } set{ _stageScoreBoard.text = value; }}
	public StoreUiController StoreUiController {get { return _storeUiController; }}


	string test = null;

	void Start () 
	{
		_stopwatch.text = "0";
		_points.text = "0";
		UpdateCoins();

	}

	void Update () 
	{

	}

	public void ClearStates()
	{
		SetStates (Splash, false);
		SetStates (ScoreBoard, false);
//		SetStates (Pause, false);
		SetStates (Game, false);
		SetStates (GameOver, false);
	}

	public void SetState(IGameState state)
	{
		SetStates (Splash, state == GameStateController.States.Splash);
		SetStates (ScoreBoard, state == GameStateController.States.Scoreboard);
//		SetStates (Pause, state == GameStateController.States.Pause);
		SetStates (Game, state == GameStateController.States.Game); 
		SetStates (GameOver, state == GameStateController.States.GameOver); 
	}

	void SetStates(GameObject element, bool active)
	{
		element.gameObject.SetActive (active);
	}

	public void showPausePanel(){
		Pause.SetActive (true);
	}
	public void hidePausePanel(){
		Pause.SetActive (false);
	}

	public void UpdateCoinsPosition(){
		CoinsObject.GetComponent<Animator> ().SetBool ("FBIsOn", FacebookManager.Instance.IsLoggedIn);
	}
		

	int levelsPage = 0;
	int maxLevelPage = 2;
	public void UpdateLevelsPanel(){
		int clearedLavels = GameSave.getClearedLevels ();
		for (int i = 0; i < 8; i++) {
			int stageNumber = levelsPage * 8 + i;
			StagesPanels [i].setStageInfo (stageNumber+1, GameSave.GetScore (stageNumber), stageNumber > clearedLavels);
		}
		if (levelsPage >= maxLevelPage) {
			NextPageBtn.SetActive (false);
		} else {
			NextPageBtn.SetActive (true);
		}
		if (levelsPage <= 0) {
			PreviousPageBtn.SetActive (false);
		} else {
			PreviousPageBtn.SetActive (true);
		}
	}

	public void SelectStage(int stageNumber){
		stageNumber = levelsPage * 8 + stageNumber;
		GameStateController.Instance.SelectStage (stageNumber);
		HideLevelSelectMenu ();
	}
		

	public void NextLevelPage(){
		levelsPage++;
		UpdateLevelsPanel ();
	}
	public void PreviousLevelPage(){
		levelsPage--;
		if (levelsPage < 0)
			levelsPage = 0;
		UpdateLevelsPanel ();
	}

	public void ShowLevelSelectMenu(){
		LevelsPanel.SetActive (true);
		levelsPage = 0;
		UpdateLevelsPanel ();
	}
	public void HideLevelSelectMenu(){
		LevelsPanel.SetActive (false);
	}
	public void ShowStore(){
		StoreUiController.EnableItemsPanel ();
		StorePanel.SetActive (true);

	}
	public void HideStore(){
		StorePanel.SetActive (false);
	}
	public void UpdateCoins(){
		_coins.text = GameSave.getCoins ().ToString();
		_coinsStore.text = GameSave.getCoins ().ToString();
	}
	public void ShowTutorial(){
		GameSave.SetTutorial ();
		LevelsPanel.SetActive (false);
		Tutorial.SetActive (true);
	}
	public void HideTutorial(){
		Tutorial.SetActive (false);
	}
	public void UpdateFacebookBlink(){
		FacebookMenuBtnBlock.SetActive (FacebookManager.Instance.IsLoggedIn);
		if (FacebookManager.Instance.IsLoggedIn) {
			
//			FacebookButton.GetComponent<Animator> ().SetBool ("WeakBlink",false);
			FacebookButton.SetActive(false);
		} else {
			FacebookButton.SetActive(true);
			FacebookButton.GetComponent<Animator> ().SetBool ("WeakBlink",true) ;

		}
	}



	public void SettingsCallback(){
		MenuSettings.GetComponent<Animator>().SetBool("Show", !MenuSettings.GetComponent<Animator>().GetBool("Show"));
	}



}
