﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class LineRendererSkinRule
{
	[SerializeField] float _startWidth = 1;
	[SerializeField] float _endWidth = 1;
	[SerializeField] Color _startColor = Color.white;
	[SerializeField] Color _endColor = Color.white;
	
	public float StartWidth { get { return _startWidth; } }
	public float EndWidth { get { return _endWidth; } }
	public Color StartColor { get { return _startColor; } }
	public Color EndColor { get { return _endColor; } }
	
	public LineRendererSkinRule() : this( 1, 1, Color.white, Color.white )	{ }
	public LineRendererSkinRule( float startWidth, float endWidth, Color startColor, Color endColor )
	{
		this._startWidth = startWidth;
		this._endWidth = endWidth;
		this._startColor = startColor;
		this._endColor = endColor;
	}	
	
	public void Apply( LineRenderer path )
	{
		path.SetWidth( _startWidth, _endWidth );
		path.SetColors( _startColor, _endColor );
	}
	
	public static LineRendererSkinRule Blend( LineRendererSkinRule a, LineRendererSkinRule b, float value )
	{
		var sw = Mathf.Lerp( a.StartWidth, b.StartWidth, value );
		var ew = Mathf.Lerp( a.EndWidth, b.EndWidth, value );
		var sc = Color.Lerp( a.StartColor, b.StartColor, value );
		var ec = Color.Lerp( a.EndColor, b.EndColor, value );
		var ret = new LineRendererSkinRule( sw, ew, sc, ec );
		return ret;
	}
}
