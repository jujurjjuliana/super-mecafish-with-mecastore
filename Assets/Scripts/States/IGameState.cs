﻿using UnityEngine;
using System.Collections;

public interface IGameState 
{
	bool isAtive { get; }

	ECameraState CameraState { get; }

	void Enter(IGameStateController  gameController);
	void Update(IGameStateController  gameController, float deltaTime);
	void Exit(IGameStateController  gameController);
}
