﻿using UnityEngine;
using System.Collections;

public class GameOver : BaseGameState 
{
	public override ECameraState CameraState { get { return ECameraState.GAME; } }

	public override void Enter(IGameStateController  gameController)
	{
		gameController.Ui.StageGameOver = gameController.Ui.Stage;//((int)gameController.Director.SceneNumber + 1).ToString();

		gameController.Ui.GameOverPts = ((int)gameController.Nature.QtdOilRemoved).ToString ();

		gameController.Nature.Clear ();

		if (gameController.Nature.OilCount > 0)
			gameController.Nature.Clear ();
		

		if (EnableInput.Pause) 
		{
			Time.timeScale = 0;
			EnableInput.Pause = false;
		}
		else
		{
			EnableInput.Pause = true;
			Time.timeScale = 1;	
		}


		/*if (gameController.Nature.OilCount > 0)
			gameController.Nature.Clear ();
		else
			Debug.Log ("nao tem oleo para ser removido");*/

		//gameController.Ui.Points = (gameController.Nature.QtdOilRemoved).ToString();
	}

	public override void Update(IGameStateController  gameController, float deltaTime)
	{

	}

	public override void Exit(IGameStateController  gameController)
	{
		
		//gameController.Nature.Clear ();

		if (EnableInput.Pause) 
		{
			Time.timeScale = 0;
			EnableInput.Pause = false;
		}
		else
		{
			EnableInput.Pause = true;
			Time.timeScale = 1;	
		}

		Harbor.Instance.ResetFishes ();

	}

}
