﻿using UnityEngine;
using System.Collections;

public class PauseState  : BaseGameState 
{
	public override ECameraState CameraState { get { return ECameraState.GAME; } }

	public override void Enter(IGameStateController  gameController)
	{
		Time.timeScale = 0;
		EnableInput.Pause = false;
	}
	
	public override void Update(IGameStateController  gameController, float deltaTime)
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			gameController.Back ();
		}
	}
	
	public override void Exit(IGameStateController  gameController)
	{
		Time.timeScale = 1;
		EnableInput.Pause = true;
	}
}
