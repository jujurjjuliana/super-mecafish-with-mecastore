﻿using UnityEngine;
using System.Collections;

public abstract class BaseGameState  : IGameState 
{
	bool  active = false;

	public bool isAtive {get {return  active;} }
	public abstract ECameraState CameraState { get; }

	void IGameState .Enter(IGameStateController  gameController)
	{
		 active = true;
		Enter (gameController);
	}	
	
	public abstract void Enter(IGameStateController  gameController);
	public abstract void Update(IGameStateController  gameController, float time);
	public abstract void Exit(IGameStateController  gameController);

	void IGameState .Exit(IGameStateController  gameController)
	{
		 active = false;
		Exit (gameController);
	}

}
