﻿using UnityEngine;
using System.Collections;

public class ScoreboardState  : BaseGameState 
{
	public override ECameraState CameraState { get { return ECameraState.GAME; } }

	public override void Enter(IGameStateController  gameController)
	{
		gameController.Ui.StageScoreBoard = gameController.Ui.Stage;
		int gameScore = (int)gameController.Nature.QtdOilRemoved;
		gameScore += (int) (gameController.Director.Scene.FailCondition.MaxValue- (int) gameController.Director.Scene.FailCondition.Value)*15;
		gameController.Ui.ScoreBoardpts = gameScore.ToString ();
		int gameCoins = gameScore / 20;
		gameController.Ui.ScoreBoardCoins = gameCoins.ToString ();

		int bonusCoins = (int) (gameCoins * Harbor.Instance._launched [0].GetCoinRateBonus ());
		if (bonusCoins > 0) {
			gameController.Ui.ScoreBoardBonusGroup.SetActive (true);
			gameController.Ui.ScoreBoardBonusCoins = bonusCoins.ToString ();
		} else {
			gameController.Ui.ScoreBoardBonusGroup.SetActive (false);
		}

		gameController.Nature.Clear ();

		if (gameController.Nature.OilCount > 0)
			gameController.Nature.Clear ();
		//else
		//	Debug.Log ("nao tem oleo para ser removido");

		//Register new highscore and stageCleared
		int stageNumber = gameController.Director.SceneNumber;
		if (GameSave.getClearedLevels() < stageNumber+1) {
			GameSave.setClearedLevels (stageNumber+1);
		}
		if (GameSave.GetScore (stageNumber) < gameScore) {
			GameSave.SetScore (stageNumber, gameScore);
		}
		GameSave.AddCoins (gameCoins+bonusCoins);


		if (EnableInput.Pause) 
		{
			Time.timeScale = 0;
			EnableInput.Pause = false;
		}
		else
		{
			EnableInput.Pause = true;
			Time.timeScale = 1;	
		}


	}

	public override void Update(IGameStateController  gameController, float deltaTime)
	{

	}

	public override void Exit(IGameStateController  gameController)
	{
		Harbor.Instance.ResetFishes ();

		if (EnableInput.Pause) 
		{
			Time.timeScale = 0;
			EnableInput.Pause = false;
		}
		else
		{
			EnableInput.Pause = true;
			Time.timeScale = 1;	
		}
	}
}
