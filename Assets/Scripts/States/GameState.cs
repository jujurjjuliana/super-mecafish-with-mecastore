﻿using UnityEngine;
using System.Collections;

public class GameState  : BaseGameState 
{
	public override ECameraState CameraState { get { return ECameraState.GAME; } }

	public override void Enter(IGameStateController  gameController)
	{
		Debug.Log ("entered in game state");
		gameController.Director.Scene.FailCondition.BeforeRegister ();
		gameController.Director.Scene.FailCondition.Init ();

		gameController.Ui.Timer = "0";
		gameController.Ui.Stage = ((int)gameController.Director.SceneNumber + 1).ToString();
		//TODO: nao deveria ser lançado aqui, isso foi feito assim para facilitar a saida do peixe. Deve ser arrumado.\
		Harbor.Instance.Launch ();

	}	

	public override void Update(IGameStateController  gameController, float deltaTime)
	{

		if( Input.GetKeyDown( KeyCode.Escape ) )
		{
			Application.Quit();
			//gameController.GoToPause();
		}

		if (gameController.Director.Scene.Objectives.AllDone) 
		{			
			gameController.GoToScoreboard ();
		} 
		else if (!gameController.Director.Scene.FailCondition.IsValid) 
		{		
			gameController.GoToGameOver ();
		}

		gameController.Ui.Timer = (gameController.Director.Scene.FailCondition.MaxValue- (int)gameController.Director.Scene.FailCondition.Value).ToString();

		gameController.Ui.Points = ((int)gameController.Nature.QtdOilRemoved).ToString ();
	}
	
	public override void Exit(IGameStateController  gameController)
	{		
		gameController.Director.Scene.FailCondition.Unregister ();
	}
		

}
