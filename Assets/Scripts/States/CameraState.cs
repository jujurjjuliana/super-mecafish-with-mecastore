﻿using UnityEngine;
using System.Collections;

public interface ICameraState
{
	Vector3 Position { get; }
	Quaternion Rotation { get; }
	float Size { get; }
}

public class CameraState : ICameraState
{
	public CameraState( Vector3 position, Quaternion rotation, float size )
	{
		this.Position = position;
		this.Rotation = rotation;
		this.Size = size;
	}	

	public Vector3 Position { get; private set; }
	public Quaternion Rotation { get; private set; }
	public float Size { get; private set; }
}