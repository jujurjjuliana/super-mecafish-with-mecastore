﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class SplashState  : BaseGameState  
{
	public override ECameraState CameraState { get { return ECameraState.SPLASH; } }

	public override void Enter(IGameStateController  gameController)
	{
		Interaction.GatherInput.RequestButDoNotIncrease( this );
		gameController.Nature.Clear ();

		if (gameController.Nature.OilCount > 0)
			gameController.Nature.Clear ();

		gameController.Ui.UpdateCoins ();

	}



	public override void Update(IGameStateController  gameController, float deltaTime)
	{
		gameController.Ui.UpdateFacebookBlink ();
		if( Fish.All.Any( ( a ) => a.Launched ) )
		{
			//gameController.GoToGame();
		}

		if( Input.GetKeyDown( KeyCode.Escape ) )
		{
			Application.Quit();
		}
	}

	public override void Exit(IGameStateController  gameController)
	{
		Interaction.GatherInput.RemoveRequest( this );
	}
}
