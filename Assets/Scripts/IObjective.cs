﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface IObjective 
{
	bool IsDone{ get; }
	void Start(IGameNature  events);
	void Finish();
}

