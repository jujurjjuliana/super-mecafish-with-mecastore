﻿using UnityEngine;
using System.Collections;

public interface IGameScenario 
{
	void Start(IGameNature  events);
	void Finish();

	IObjectiveCollection  Objectives{ get;}
	ICondition  FailCondition { get;}
	INatureData  NatureData { get;}
}

public class GameScenario  : IGameScenario 
{
	public ICondition  FailCondition { get; private set;}
	public INatureData  NatureData { get; private set; }
	public IObjectiveCollection  Objectives { get{ return  _objectives; } } 
	Level _level;
	Level isntantiatedLevel;

	ObjectiveCollection  _objectives = new ObjectiveCollection ();

	public GameScenario (INatureData  natureData, ICondition  failcondition, params IObjective [] objectives )
	{
		this.NatureData = natureData;
		this.FailCondition = failcondition;
		_objectives.AddRange (objectives);
	}

	public GameScenario (Level  level)
	{
		this.FailCondition = new TimerCondition(level.LevelTime);
		_objectives.Add (new GetOilObjective(level.NeededTrash));
		_level = level;
	}


	public void Start(IGameNature  nature)
	{
		foreach(var obj in Objectives)
		{
			obj.Start(nature);
		}
		isntantiatedLevel = (Level) GameObject.Instantiate(_level,Vector3.zero,Quaternion.identity);
		NatureData = new NatureData (isntantiatedLevel.EnemiesList, isntantiatedLevel.Collectables);
		nature.SetData (NatureData);

	}

	public void Finish()
	{
		foreach(var obj in Objectives)
		{
			obj.Finish();
		}
		GameObject.Destroy (isntantiatedLevel.gameObject);
	}

}
