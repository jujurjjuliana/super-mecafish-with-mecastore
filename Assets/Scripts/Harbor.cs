﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Hangar
{
	public static int StarsEarned { get; set; }
	public static int StarsSpent { get; set; }
	public static int StarsBalance { get { return StarsEarned - StarsSpent; } }


	class FishCreator
	{
		static int s_planes = 0;

		public static IFishData Speed { get { return new SpeedFishData( ++s_planes ); } }
		public static IFishData Tank { get { return new TankFishData( ++s_planes ); } }
	}

	static EventSlot _poolChanged = new EventSlot();
	static EventSlot<IFishData> _fishAdded = new EventSlot<IFishData>();
	static EventSlot<IFishData> _fishRemoved = new EventSlot<IFishData>();

	public static IEventRegister OnPoolChanged { get { return _poolChanged; } }
	public static IEventRegister<IFishData> OnFishAdded { get { return _fishAdded; } }
	public static IEventRegister<IFishData> OnFishRemoved { get { return _fishRemoved; } }

	static List<IFishData> _pool = new List<IFishData>{ FishCreator.Speed };

	public static void AddSpeed() { AddFish( FishCreator.Speed ); }
	//public static void AddTank() { AddFish( FishCreator.Tank ); }

	//ordenar o _pool de acordo com o _launchLine
	public static void Reorder( List<IFish> _launchLine )
	{
		if( Hangar.Pool.Count != _launchLine.Count )
			return;

		for( int i = 0; i < _pool.Count; i++ )
		{
			if( _launchLine[i].Data != null ) 
				_pool[i] = _launchLine[i].Data;
		}
	}

	public static int Count( EFishType type )
	{
		int count = 0;

		for( int i = 0; i < _pool.Count; i++ )
		{
			if( _pool[i].Type == type ) count++;
		}

		return count;
	}

	static void AddFish( IFishData data )
	{
		_pool.Add( data );
		_poolChanged.Trigger();
		_fishAdded.Trigger( data );
	}

	public static List<IFishData> Pool { get { return _pool; } }
}


public interface IHarborEvents
{
	IEventRegister<IFish> OnLaunch { get; }
}


public class Harbor : MonoBehaviour
{
	[SerializeField] Transform[] SpawnPoints;
	[SerializeField] List<Vector3> _p = new List<Vector3>();
	[SerializeField] GameObject _go;

	static Harbor _instance = null;
	public static Harbor Instance { get { return _instance; } }

	const float MINIMUN_SPAWN_DIST = 3;
	const float MINIMUN_SPAWN_SQRTDIST = MINIMUN_SPAWN_DIST * MINIMUN_SPAWN_DIST;

	private int flag = 0;
	[SerializeField] GameObject _fishSpeed;
	[SerializeField] GameObject _fishTank;

	[SerializeField] Interaction _gameInteraction;

	[SerializeField] GameObject _runwayBegin;
	[SerializeField] GameObject _runwayEnd;
	[SerializeField] float _runwayScale = 1;
	[SerializeField] TransformBezier _fishesLine;
	//[SerializeField] RandomClipFX _launchSFX;

	[SerializeField] RunwayLightController _lights;

	bool _dirty = true;

	public float RunwayScale { get { return _runwayScale; } set { _runwayScale = value; } }
	public Vector3 RunwayEnd { get { return _runwayEnd.transform.position; } }
	public Vector3 RunwayBegin { get { return _runwayBegin.transform.position; } }

	HarborEvents _events = new HarborEvents();
	public IHarborEvents Events { get { return _events; } }

	public static int Collisions { get; set; }

	public List<IFish> _launchLine = new List<IFish>();
	public List<IFish> _launched = new List<IFish>();


	void Awake()
	{
		if( _instance != null )
		{
			GameObject.Destroy( _instance.gameObject );
		}
		_instance = this;

		//circle ();
	}

	void Start()
	{
		//	Hangar.OnFishAdded.Register( ResetFishes );
		ResetFishes();
	}

	public GameObject GetPrefab( EFishType type )
	{
		return _fishSpeed;
	}

	public void AddToLaunchLine( Fish fish )
	{
		_launchLine.Add( fish );

		System.Func<Vector3> posProvider = () => 
		{
			int id = _launchLine.IndexOf( fish );
		//	return GetLinePosition( id, _launchLine.Count );
			return SpawnPoints[0].position;
		};

		System.Func<Quaternion> rotationProvider = () => 
		{
			int id = _launchLine.IndexOf( fish );
			//return GetLineRotation( id, _launchLine.Count );
			return SpawnPoints[0].rotation;
		};

		fish.LandedData( posProvider, rotationProvider );
	}

	public void ResetFishes()
	{
		
		Collisions = 0;
		_dirty = false;

		for( int i = 0; i < _launched.Count; i++ ) 
		{
			if (_launched [i].Live) 
			{
				_launched [i].Unregister ();
				_launched[i].Kill(); 
			}
				
		}


		for( int i = 0; i < _launchLine.Count; i++ ) 
		{
			if(_launchLine[i].Live) _launchLine[i].Kill();
		}

		_launched.Clear();
		_launchLine.Clear();

		for( int i = 0; i < Hangar.Pool.Count; i++ )
		{
			var data = Hangar.Pool[i];
			var fish = RealySpawn( SpawnPoints[i].position, GetLineRotation( i, Hangar.Pool.Count ), GetPrefab( data.Type ) );

			fish.SetData( data );
			AddToLaunchLine( fish );
		}
	}

	Quaternion GetLineRotation( int id, int count )
	{
		float step = Step( count );
		var pos = _fishesLine.GetPoint( ( (float)id ) * step + .001f );
		var dpos = _fishesLine.GetPoint( ( ( (float)id ) * step ) - .001f );
		var dir = dpos - pos;

		if( dir.sqrMagnitude == 0 )
			return Quaternion.identity;

		return Quaternion.LookRotation( dir, Vector3.up );
	}

	Vector3 GetLinePosition( int id, int count )
	{
		return _fishesLine.GetPoint( ( (float)id ) * Step( count ) );
	}

	float Step( int count )
	{
		float step = ( 1f / ( count - 1 ) );
		if( step > .1f )
			step = .1f;

		return step;
	}

	public static void Reset()
	{
		_instance.ResetFishes();
	}

	public int GetClosestID( Vector3 pos )
	{
		float max = float.MaxValue;
		int id = -1;
		float step = Step( _launchLine.Count );

		for( int i = 0; i < _launchLine.Count; i++ )
		{
			var p2 = GetLinePosition( i, _launchLine.Count );
			var dist = Vector3.Distance( pos, p2 );
			if( dist < max )
			{
				max = dist;
				id = i;
			}
		}
		return id;
	}

	public void SetFish( IFish fish, int id )
	{

		var iid = _launchLine.IndexOf( fish );

		if( iid < 0 )
			return;

		int steps = Mathf.Abs( iid - id );
		int sign = (int)Mathf.Sign( iid - id );

		var aux = fish;
		var aux2 = fish;
		for( int i = 0; i <= steps; i++ )
		{
			aux = _launchLine[ id + sign * i ];
			_launchLine[ id + sign * i ] = aux2;
			aux2 = aux;
		}

		if( !_dirty )
		{
			Hangar.Reorder( _launchLine );
		}
	}

	public IFish Launch()
	{
		_dirty = true;
		if( _launchLine.Count == 0 )
			return null;

		IFish fish = _launchLine[ 0 ];
		_launchLine.RemoveAt( 0 );
		fish.Launch();
		_launched.Add( fish );

		_events.Launch.Trigger( fish );

		return fish;
	}


	public void DragBegin() { _lights.DragBegin(); }	
	public void DragUpdate( float f ) { _lights.DragUpdate( f ); }	
	public void DragEnd() { _lights.DragEnd(); }

	void Update () { }

	Fish RealySpawn( Vector3 pos, Quaternion dir, GameObject reference )
	{
		var go = (GameObject)GameObject.Instantiate( reference, pos, Quaternion.identity );
		var fish = go.GetComponent<Fish>();
		//fish.SetPathSkin( _gameInteraction._unselectedPath );
		return fish;
	}

	float SqrtDistance2D( Vector3 posA, Vector3 posB )
	{
		var x = posA.x - posB.x;
		var y = posA.y - posB.y;
		return x * x + y * y;
	}

	public void OnDrawGizmos()
	{
		if( _runwayBegin == null || _runwayEnd == null )
			return;

		float scale = _runwayScale;
		var pb = _runwayBegin.transform.position;
		var pe = _runwayEnd.transform.position;
		var dir = ( pe - pb ).normalized;
		var cross = Vector3.Cross( dir, Vector3.up );
		//Debug.DrawLine( _runwayBegin.transform.position + cross * scale, _runwayEnd.transform.position + cross * scale, Color.red );
		//Debug.DrawLine( _runwayBegin.transform.position - cross * scale, _runwayEnd.transform.position - cross * scale, Color.red );
		var initial = cross;
		float step = 10f;

		for( float a = step; a <= 360; a += step )
		{
			var current = Quaternion.Euler( 0, step, 0 ) * initial;
			//Debug.DrawLine( _runwayBegin.transform.position - current * scale, _runwayBegin.transform.position - initial * scale, Color.red );
			//Debug.DrawLine( _runwayEnd.transform.position + current * scale, _runwayEnd.transform.position + initial * scale, Color.red );
			Debug.DrawLine( _runwayBegin.transform.position + current * scale, _runwayBegin.transform.position + initial * scale, Color.red );

			//Debug.Log (_runwayBegin.transform.position + current * scale + " " + _runwayBegin.transform.position + initial * scale );

			//Debug.DrawLine( _runwayEnd.transform.position - current * scale, _runwayEnd.transform.position - initial * scale, Color.red );
			initial = current;
		}

	}


	public void circle()
	{
		if( _runwayBegin == null || _runwayEnd == null )
			return;

		float scale = _runwayScale;
		var pb = _runwayBegin.transform.position;
		var pe = _runwayEnd.transform.position;
		var dir = ( pe - pb ).normalized;
		var cross = Vector3.Cross( dir, Vector3.up );
		//Debug.DrawLine( _runwayBegin.transform.position + cross * scale, _runwayEnd.transform.position + cross * scale, Color.red );
		//Debug.DrawLine( _runwayBegin.transform.position - cross * scale, _runwayEnd.transform.position - cross * scale, Color.red );
		var initial = cross;
		float step = 10f;

		for( float a = step; a <= 360; a += step )
		{
			var current = Quaternion.Euler( 0, step, 0 ) * initial;
			//Debug.DrawLine( _runwayBegin.transform.position - current * scale, _runwayBegin.transform.position - initial * scale, Color.red );
			//Debug.DrawLine( _runwayEnd.transform.position + current * scale, _runwayEnd.transform.position + initial * scale, Color.red );
			Debug.DrawLine( _runwayBegin.transform.position + current * scale, _runwayBegin.transform.position + initial * scale, Color.red );

			Instantiate(_go, _runwayBegin.transform.position + current * scale, Quaternion.identity);

			_p.Add (_runwayBegin.transform.position + current * scale);

			//Debug.Log (_runwayBegin.transform.position + current * scale + " " + _runwayBegin.transform.position + initial * scale );

			//Debug.DrawLine( _runwayEnd.transform.position - current * scale, _runwayEnd.transform.position - initial * scale, Color.red );
			initial = current;
		}
	}


	class HarborEvents : IHarborEvents
	{
		EventSlot<IFish> _launch = new EventSlot<IFish>();

		public IEventRegister<IFish> OnLaunch { get { return _launch; } }

		public IEventTrigger<IFish> Launch { get { return _launch; } }
	}

}
