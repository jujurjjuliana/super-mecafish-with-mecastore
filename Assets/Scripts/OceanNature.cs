using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*public static class EnemyCreator
{
	static int s_enemies = 0;

	public static IEnemyData BlackEnemy { get { return new BlackEnemyData (++s_enemies); } }
	public static IEnemyData RedEnemy { get { return new RedEnemyData (++s_enemies); } }
	public static IEnemyData GreenEnemy { get { return new GreenEnemyData (++s_enemies); } }

}*/



public class OceanNature: MonoBehaviour, IGameNature
{
	class GameEventBroadcaster: IGameEventBroadcaster
	{
		class OilEventBroadcaster: IOilEventBroadcaster, IOilEventBroadcasterTrigger
		{
			EventSlot<ILogicOil> _start = new EventSlot<ILogicOil> ();
			EventSlot<ILogicOil> _getOil = new EventSlot<ILogicOil> ();
			EventSlot<ILogicOil> _increase = new EventSlot<ILogicOil> ();

			public IEventRegister<ILogicOil> Start { get {return _start;} }
			public IEventRegister<ILogicOil> GetOil { get {return _getOil; } }
			public IEventRegister<ILogicOil> Increase { get {return _increase; } }

			public void TriggerStart(ILogicOil oil) { _start.Trigger (oil);}
			public void TriggerGetOil(ILogicOil oil) {_getOil.Trigger (oil);}
			public void TriggerIncrease(ILogicOil oil) {_increase.Trigger (oil);}
		}

		class GreenEnemyBroadcaster: IGreenEnemyEventBroadcaster, IGreenEnemyEventBroadcasterTrigger
		{			
			EventSlot<IEnemy> _fishContact = new EventSlot<IEnemy>();

			public IEventRegister<IEnemy> FishContact { get { return _fishContact; } }

			public void TriggerFishContact(IEnemy enemy) { _fishContact.Trigger(enemy);}

		}


		class RedEnemyBroadcaster: IRedEnemyEventBroadcaster, IRedEnemyEventBroadcasterTrigger
		{			
			EventSlot<IEnemy> _fishContact = new EventSlot<IEnemy>();

			public IEventRegister<IEnemy> FishContact { get { return _fishContact; } }

			public void TriggerFishContact(IEnemy enemy) { _fishContact.Trigger(enemy);}

		}

		class BlackEnemyBroadcaster: IBlackEnemyEventBroadcaster, IBlackEnemyEventBroadcasterTrigger
		{			
			EventSlot<IEnemy> _fishContact = new EventSlot<IEnemy>();

			public IEventRegister<IEnemy> FishContact { get { return _fishContact; } }

			public void TriggerFishContact(IEnemy enemy) { _fishContact.Trigger(enemy);}

		}

		class FishEnemyBroadcaster: IFishEnemyEventBroadcaster, IFishEnemyEventBroadcasterTrigger
		{			
			EventSlot<int> _fishContact = new EventSlot<int>();

			public IEventRegister<int> FishContact { get { return _fishContact; } }

			public void TriggerFishContact(int t) { _fishContact.Trigger(t);}

		}
		class MachineBroadcaster: IMachineRecicleEventBroadcaster, IMachineRecicleEventBroadcasterTrigger
		{			
			EventSlot<int> _machineRecicle = new EventSlot<int>();

			public IEventRegister<int> MachineRecicle { get { return _machineRecicle; } }

			public void TriggerMachineRecicle(int t) { _machineRecicle.Trigger(t);}

		}


		OilEventBroadcaster _oilEvents = new OilEventBroadcaster ();
		public IOilEventBroadcaster Oil {get{return _oilEvents;}}
		public IOilEventBroadcasterTrigger OilTrigger {get{return _oilEvents;}}

		GreenEnemyBroadcaster _greenEnemyEvents = new  GreenEnemyBroadcaster ();
		public IGreenEnemyEventBroadcaster GreenEvent { get { return _greenEnemyEvents; } }
		public IGreenEnemyEventBroadcasterTrigger GreenEventTrigger {get { return _greenEnemyEvents; }}

		RedEnemyBroadcaster _redEnemyEvents = new  RedEnemyBroadcaster ();
		public IRedEnemyEventBroadcaster RedEvent { get { return _redEnemyEvents; } }
		public IRedEnemyEventBroadcasterTrigger RedEventTrigger {get { return _redEnemyEvents; }}
	
		FishEnemyBroadcaster _fishEnemyEvents = new  FishEnemyBroadcaster ();
		public IFishEnemyEventBroadcaster FishEvent { get { return _fishEnemyEvents; } }
		public IFishEnemyEventBroadcasterTrigger FishEventTrigger {get { return _fishEnemyEvents; }}

		BlackEnemyBroadcaster _blackEnemyEvents = new  BlackEnemyBroadcaster ();
		public IBlackEnemyEventBroadcaster BlackEvent { get { return _blackEnemyEvents; } }
		public IBlackEnemyEventBroadcasterTrigger BlackEventTrigger {get { return _blackEnemyEvents; }}

		MachineBroadcaster _machineRecicleEvents = new  MachineBroadcaster ();
		public IMachineRecicleEventBroadcaster MachineEvent { get { return _machineRecicleEvents; } }
		public IMachineRecicleEventBroadcasterTrigger MachineEventTrigger {get { return _machineRecicleEvents; }}

	}

	const float MAXIMUM_EXPAND_DISTANCE = 3;
	const float MAXIMUM_EXPAND_SQUARED_DISTANCE = MAXIMUM_EXPAND_DISTANCE * MAXIMUM_EXPAND_DISTANCE;
	const float OIL_INTERVAL = 5;
	//const float EXPLOSION_RADIUS = 1;
	//const float EXPLOSION_SQRT_RADIUS = EXPLOSION_RADIUS * EXPLOSION_RADIUS;
	const float OIL_DISTANCE_TO_GET_OIL = 1;
	const float OIL_SQRT_DISTANCE_TO_GET_OIL = OIL_DISTANCE_TO_GET_OIL * OIL_DISTANCE_TO_GET_OIL;

	float _oilTime;
	
	GameEventBroadcaster _events = new GameEventBroadcaster();
	public IGameEventBroadcaster Events { get {return _events;} }

	[SerializeField] GameRegions _regions;
	[SerializeField] public  int _semaphore;


	List<LogicOil> _oil = new List<LogicOil>();
	public List<LogicOil> Oil { get { return _oil; } }
	public int OilCount{ get{return _oil.Count;}}


	[SerializeField] int _removedOil;
	public int QtdOilRemoved{get{ return _removedOil; } set{_removedOil = value;} }

	IList<Fish> _fish;


	[SerializeField] GameObject _blackEnemy;
	[SerializeField] GameObject _redEnemy;
	[SerializeField] GameObject _greenEnemy;
	[SerializeField] GameObject _brokenEnemy;

	IEnemyData _data;

	static int s_enemies = 0;

	void Awake()
	{ 	
		_regions = Guaranteed<GameRegions>.Instance;

	}

	void Start()
	{
		_fish = Fish.All;
	}

	public void Clear()
	{
		
		var enemy = Enemy.All;

		for (int i = 0; i < enemy.Count; ++i) 
		{
			enemy [i].Kill ();
		}
	
	}


	void Update()
	{
		var delta = Time.deltaTime;

		for (int i=0; i < _oil.Count; ++i) 
		{
			var oil = _oil[i];

		}
	}


	public void SetData(INatureData natureData)
	{
		_removedOil = 0;


		WorldOil (natureData.EnemyList,natureData.CollectableList);
	}


	public GameObject GetPrefab(EEnemyType type)
	{
		if (type == EEnemyType.BLACK_ENEMY)	return _blackEnemy;
		else if (type == EEnemyType.RED_ENEMY) return _redEnemy;
		else if (type == EEnemyType.GREEN_ENEMY) return _greenEnemy;
		else if (type == EEnemyType.BROKEN_ENEMY) return _brokenEnemy;	

		return null;
	}


	Enemy ReallySpawn(Vector3 pos, Quaternion dir, GameObject reference)
	{
		var go = (GameObject)GameObject.Instantiate (reference, pos, dir);
		var enemy = go.GetComponent<Enemy> ();

		return enemy;
	}


	public void WorldOil (List<Enemy> enemyList,List<Enemy> collectableList)
	{
		//enemies
		for (int i = 0; i < enemyList.Count; ++i) 
		{
			var enemy = enemyList[i];

			if (enemyList[i].Data.Type == EEnemyType.BLACK_ENEMY) 
			{
				enemy.GetComponent<BlackEnemy> ().eventTrigger = _events.BlackEventTrigger;
			}
			else if (enemyList[i].Data.Type == EEnemyType.RED_ENEMY) 
			{
				enemy.GetComponent<RedEnemy> ().eventTrigger = _events.RedEventTrigger;
			}
			else if (enemyList[i].Data.Type == EEnemyType.GREEN_ENEMY) 
			{				
				enemy.GetComponent<GreenEnemy> ().eventTrigger = _events.GreenEventTrigger;
			}
			enemyList [i].Data.SetDataInfo (++s_enemies, enemyList[i].transform.position);
		}
		//colectables
		for (int i = 0; i < collectableList.Count; ++i) 
		{
			var enemy = collectableList[i];

			if (collectableList[i].Data.Type == EEnemyType.BLACK_ENEMY) 
			{
				enemy.GetComponent<BlackEnemy> ().eventTrigger = _events.BlackEventTrigger;
			}
			else if (collectableList[i].Data.Type == EEnemyType.RED_ENEMY) 
			{
				Debug.Log (enemy);
				enemy.GetComponent<RedEnemy> ().eventTrigger = _events.RedEventTrigger;
			}
			else if (collectableList[i].Data.Type == EEnemyType.GREEN_ENEMY) 
			{				
				enemy.GetComponent<GreenEnemy> ().eventTrigger = _events.GreenEventTrigger;
			}
			collectableList [i].Data.SetDataInfo (++s_enemies, collectableList[i].transform.position);
		}


	}


	public bool CreateEnemy(Vector3 pos, EEnemyType type, float speed)
	{
		var enemy = ReallySpawn (pos, Quaternion.identity, GetPrefab (type));

		if (type == EEnemyType.BLACK_ENEMY) 
		{
			_data = new BlackEnemyData (++s_enemies, pos, speed);
			enemy.GetComponent<BlackEnemy> ().eventTrigger = _events.BlackEventTrigger;
		}
		else if (type == EEnemyType.RED_ENEMY) 
		{
			enemy.GetComponent<RedEnemy> ().eventTrigger = _events.RedEventTrigger;
			_data = new RedEnemyData (++s_enemies, pos, speed);
		}
		else if (type == EEnemyType.GREEN_ENEMY) 
		{
			enemy.GetComponent<GreenEnemy> ().eventTrigger = _events.GreenEventTrigger;
			_data = new GreenEnemyData (++s_enemies, pos, speed);
		}
		else if(type == EEnemyType.BROKEN_ENEMY)
		{
			_data = new BrokenEnemyData (++s_enemies, pos, speed);
		}

		enemy.SetData (_data);

		if (enemy != null)
			return true;

		return false;
	}


	public bool CheckFishContact(Vector3 position, EEnemyType type)
	{
		var fish = Fish.All;
		//var enemy = Enemy.All ();

//		if (_regions.BelowFish (position)) 
//		{
			for (int i = 0; i < fish.Count; ++i)
			{
			var distance = position - fish [i].transform.position; //SqrtDistance2D(position, fish[i].transform.position);
			distance.z = 0;

			if (distance.magnitude < 1.5f && type == EEnemyType.BLACK_ENEMY && !fish [i].Full) {
					return true;
			} else if (distance.magnitude< 1.9f && type == EEnemyType.RED_ENEMY) {
					return true;
			} else if (distance.magnitude < 1 && type == EEnemyType.GREEN_ENEMY && !fish [i].Full)
					return true;


			}
//		}

		return false;
	}

	public void Remove(Vector3 position, EEnemyType type, IEnemyData data, GameObject go)
	{		
		Destroy (go);
	}



	Vector3 RandomPosAt( Rect r )
	{
		return new Vector3( Random.Range( r.xMin, r.xMax ), 0, Random.Range( r.yMin, r.yMax ) );
	}




	float SqrtDistance2D( Vector3 posA, Vector3 posB )
	{
		var x = posA.x - posB.x;
		var y = posA.y - posB.y;
		return x * x + y * y;
	}


	public bool CanGetOil(Vector3 position)
	{
		for( int i = 0; i < _oil.Count; i++ )
		{
			var oil = _oil[i];
			var pos = oil.Position;
			var sqrtDist = SqrtDistance2D( pos, position );
			
			if( sqrtDist < OIL_SQRT_DISTANCE_TO_GET_OIL )
			{
				oil.GetOil();

				Debug.Log ("yes");

				Destroy (oil.Go);
				_oil.RemoveAt( i );
				
				//_removedOil++;
				
				return true;
			}
		}

		return false;
	}



	public void delete(Vector3 position)
	{	

		for (int i = 0; i < _oil.Count; i++) 
		{
			var oil = _oil [i];
			var pos = oil.Position;

			if (pos == position) 
			{
				
				Destroy (oil.Go);
				_oil.RemoveAt( i );
			}			
		}
	
	}




}

