﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// Debug helper.
/// This class is intended to offer several debugging tools for the game.
/// It controls some debugging panels and other components that can be used to debug easier
/// </summary>
public class DebugHelper : MonoBehaviour {


	[SerializeField] GameObject DebugPanel;
	[SerializeField] GameObject DebugConsole;
	[SerializeField] GameObject DebugButtonsPanel;
	[SerializeField] Text debugLog;
	public static Text logField;


	// Use this for initialization
	void Start () {
		DebugHelper.logField = debugLog;
	//	if (Debug.isDebugBuild) {
	//		DebugPanel.SetActive (true);
	//	}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	/// <summary>
	/// Toggle the appearence of the console game special console
	/// </summary>
	public void ConsoleButtonCallback(){
		DebugConsole.SetActive (!DebugConsole.activeSelf);
	}
	/// <summary>
	/// Toggle the appearence of the a panel with buttons used to help in the debugging process
	/// </summary>
	public void DegubButtonsButtonCallback(){
		DebugButtonsPanel.SetActive (!DebugButtonsPanel.activeSelf);
	}

	/// <summary>
	/// Clears the console.
	/// </summary>
	public void ClearConsole(){
		debugLog.text = "";
	}

	public static void Log(string log){
		if (logField != null) {
			logField.text = logField.text + log + "\n";
		}
		#if UNITY_EDITOR
		Debug.Log (log);
		#endif
	}

	/// <summary>
	/// Add 1000 coins to the player.
	/// </summary>
	public void add1000Coins(){
		GameSave.setCoins (GameSave.getCoins ()+ 1000);
		GameStateController.Instance.Ui.UpdateCoins();
	}

	/// <summary>
	/// Clears the players data.
	/// </summary>
	public void clearData(){
		GameSave.clear();
		GameStateController.Instance.Ui.UpdateCoins();
	}

}
