﻿using UnityEngine;
using System.Collections;

public class GetOilObjective: BaseObjective 
{
	int  _oilToPour = 1;
	int  _oil;
	///IEventTrigger <ILogicOil>  _event;
	/// 

	IEventTrigger<IEnemy> _greenEvent;
	IEventTrigger<int> _fishTankRelease;
	IEventTrigger<int> _machineRecicle;

	IEventTrigger<IEnemy> _blackEvent;

	public int OilCount{get{return _oil;}}

	public  GetOilObjective(int oilToPour)
	{
		// _event = new DelegateEventListener <ILogicOil> (GetOil);
		_greenEvent = new DelegateEventListener<IEnemy>(GetOil);
		_blackEvent = new DelegateEventListener<IEnemy>(GetOil);


		_fishTankRelease = new DelegateEventListener<int> (Test);
		_machineRecicle = new DelegateEventListener<int> (MachineRecicle);

		_oilToPour = oilToPour;
	}


	void GetOil( /*ILogicOil*/IEnemy oil )
	{
		// _oil++;

		//Debug.Log ("oil = " + _oil);
	}

	void Test(int t)
	{
		_oil++;
		Debug.Log (" Oil removed " + _oil);
	}
	void MachineRecicle(int t)
	{
		_oil++;
		Debug.Log (" Oil removed " + _oil);
	}


	void Reset()
	{
		 _oil = 0;
	}

	public override void Register( IGameNature  nature )
	{
		Reset();
		//nature.Events.Oil.GetOil.Register(  _event );

		//esse objetivo deve ser trocado: ele só vai ser completo depois que o peixe descarregar o seu tanque
		// de acordo com o numero de inimigos na cena
		nature.Events.GreenEvent.FishContact.Register (_greenEvent);
		nature.Events.BlackEvent.FishContact.Register (_blackEvent);

		nature.Events.FishEvent.FishContact.Register (_fishTankRelease);
		nature.Events.MachineEvent.MachineRecicle.Register (_machineRecicle);


	}
	
	public override void Unregister( IGameNature  nature )
	{
		//nature.Events.Oil.GetOil.Unregister ( _event);
		nature.Events.GreenEvent.FishContact.Unregister(_greenEvent);
		nature.Events.BlackEvent.FishContact.Unregister(_blackEvent);

		nature.Events.FishEvent.FishContact.Unregister (_fishTankRelease);
		nature.Events.MachineEvent.MachineRecicle.Unregister (_machineRecicle);

	}

	public override bool IsDone { get { return  _oil >= _oilToPour; } }
}