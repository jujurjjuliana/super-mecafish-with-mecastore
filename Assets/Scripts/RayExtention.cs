using UnityEngine;


public static class RayExtention 
{
	public static Vector3 GetWithY( this Ray ray, float y ) 
	{
		var distance = ( ( y - ray.origin.y ) /  ray.direction.y );
		return ray.origin + ( ray.direction * distance );
	}
	public static Vector3 GetWithZ( this Ray ray, float z ) 
	{
		var distance = ( ( z - ray.origin.z ) /  ray.direction.z );
		return ray.origin + ( ray.direction * distance );
	}
}