﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class LineRendererBlend
{
	[SerializeField] float _pathTransitionSeconds = .5f;
	float _acctime = 0;
	LineRendererSkinRule _oldRule;
	LineRendererSkinRule _newRule;
	
	public void SetNewRule( LineRendererSkinRule rule )
	{
		_oldRule = CurrentRule;
		_newRule = rule;
		_acctime = 0;
	}
	
	LineRendererSkinRule CurrentRule 
	{
		get
		{
			LineRendererSkinRule rule = null;
			if( _oldRule != null && _newRule != null )
			{
				rule = LineRendererSkinRule.Blend( _oldRule, _newRule, CurrentValue );
			}
			else
			{
				rule = new LineRendererSkinRule();
			}
			return rule;
		}
	}
	
	float CurrentValue { get { return _acctime / _pathTransitionSeconds; } }
	
	public void Update( LineRenderer path, float delta )
	{
		if( _oldRule == null || _newRule == null )
		{
			return;
		}
		
		if( _acctime < _pathTransitionSeconds )
		{
			_acctime += delta;
			if( _acctime > _pathTransitionSeconds )
			{
				_acctime = _pathTransitionSeconds;
				_oldRule = _newRule;
			}
		}
		
		var rule = CurrentRule;
		rule.Apply( path );
	}
}
