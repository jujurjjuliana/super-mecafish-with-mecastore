using System;
using UnityEngine;

namespace Constants
{
	public static class Layer
	{
		public static int BUILDINGS = UnityEngine.LayerMask.NameToLayer( "Buildings" );
		public static int FISHES = UnityEngine.LayerMask.NameToLayer( "Fishes" );
		public static int OIL = UnityEngine.LayerMask.NameToLayer("OilSource");
		public static int DEPOSITS = UnityEngine.LayerMask.NameToLayer("OilDeposit");

		public static int BLACK_ENEMY = UnityEngine.LayerMask.NameToLayer("BlackEnemy");
		public static int GREEN_ENEMY = UnityEngine.LayerMask.NameToLayer("GreenEnemy");
		public static int RED_ENEMY = UnityEngine.LayerMask.NameToLayer("RedEnemy");
	}

	public static class Measures
	{
		public static Vector3 FISH_PATH_DESLOCATION = Vector3.down * .01f;
		public static Vector3 WindDir = ( Vector3.right + Vector3.forward ) * Mathf.Sqrt( .5f );
	}

	public static class Design
	{
		public const float SECONDS_TO_FILL_OIL_TANK = 1;
	}
}

