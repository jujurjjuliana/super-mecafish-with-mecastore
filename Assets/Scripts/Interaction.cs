﻿using UnityEngine;
using System.Collections;

public class Interaction : MonoBehaviour 
{	
	static ReverseSemaphore _active = new ReverseSemaphore();
	public static IReverseSemaphore GatherInput { get { return _active; } }

	static Semaphore _canLaunch = new Semaphore();
	public static ISemaphore CanLaunch { get { return _canLaunch; } }


	[SerializeField] Camera _camera;
	public LineRendererSkinRule _unselectedPath;
	[SerializeField] LineRendererSkinRule _selectedPath;

	[SerializeField] GameObject _fishSpeed;


	static int s_fishes = 0;

	public SpeedFishData _speed = new SpeedFishData(++s_fishes);


	private int LayerCenter;
	public EnableInput input;

	int cont;
	Vector3 proj2;
	Vector3 point2;

	bool flag = false;
	public bool flag2 = false;
	public bool now = false;
	public bool center = false;

	GameRegions _regions;

	Vector3 _center;
	Vector3 _point;
	[SerializeField] Fish bestFish;

	bool borda = false;

	void Start() 
	{
		if( _camera == null )
			_camera = Camera.main;

		var fishes = Fish.All;

		for( int i = 0; i < fishes.Count; i++ )
		{
			var fish = fishes[i];
			fish.SetPathSkin( _unselectedPath );
		}

		Guaranteed<GestureInterpreter>.Instance.DragEvents.Subscribe( OnDragBegin );

		_regions = Guaranteed<GameRegions>.Instance;
	}


	public static Fish GetBestFish( Ray ray )
	{	
		Fish bestFish = null;
		var best = float.MaxValue;

		var fishes = Fish.All;		
		for( int i = 0; i < fishes.Count; i++ )
		{
			var fish = fishes[i];
			var dist = Algorithm.DistanceToRay( ray,fish.transform.position );

			if( dist < fish.TouchRadius && dist < best )
			{
				bestFish = fish;
				best = dist;
			}
		}

		return bestFish;
	}

	bool OnDragBegin( DragManager.DragGesture drag )
	{	
		RaycastHit hit;
		Ray ray = _camera.ScreenPointToRay( drag.FirstPoint );

		bestFish = null;
		bestFish = GetBestFish( ray );

		if (bestFish != null )
		{
			bestFish.launched = true;
			CommandFishPath (drag, bestFish, Vector3.zero);
			return true;
		} 


		if (_canLaunch.Free) 
		{					
			var harbor = Harbor.Instance;
			var distBegin = Algorithm.DistanceToRay( ray, harbor.RunwayBegin );
				
			if (distBegin < harbor.RunwayScale) 
			{
				var distRef = Algorithm.DistanceToRay( ray, harbor.RunwayEnd );
				bool used = false;
				harbor.DragBegin();

				drag.OnDrag += ( d )
					=> {
					if( used )
						return;
					var newRay = _camera.ScreenPointToRay( drag.LastPoint );
					var dist = Algorithm.DistanceToRay( newRay, harbor.RunwayEnd );
					harbor.DragUpdate( 1 - ( dist / distRef ) );

					if( dist < harbor.RunwayScale )
					{

						harbor.DragEnd();
						used = true;
						var fish = harbor.Launch();
						if( fish != null ) CommandFishPath( drag, fish, Vector3.zero );
					}
				};

				drag.OnDragComplete += ( d )
					=> {
					harbor.DragEnd();
				
				};

				return true;
			}
		}

		return false;
	}

	bool InsideBlockedRegion(Vector3 point)
	{
		var pos = new Vector2 (point.x, point.y);

		var pathBlockedRegion = _regions.PathBlockedRegion;

		if ( pathBlockedRegion.Contains (pos))
			return true;

		return false;

	}

	void CommandFishPath( DragManager.DragGesture drag, IFish fish, Vector3 error )
	{
		fish.SetPathSkin( _selectedPath );
		fish.ClearOrders();
		bool landed = false;

		Harbor bestHarbor = null;

		drag.OnDrag += ( d )
			=> {
			if( landed )
				return;

			var newRay = _camera.ScreenPointToRay( drag.LastPoint );

			var proj = newRay.GetWithZ( 0 );

			//nao deixa traçar rota por cima do Storage
			RaycastHit hit;

			if(Physics.Raycast(newRay, out hit))
			{
				if(hit.collider.gameObject.name == "Storage" || hit.collider.gameObject.name == "Storage2" || InsideBlockedRegion(proj))
				{			
					EnableInput.Input = false;
					bestFish = null;
				}			
				else 
				{
					
					EnableInput.Input = true;
				}
			}

			//entrance of the OilStorage
			var validRegion0 = _regions.OilStorageEntrance;


			var p =  new Vector2( proj.x - error.x, proj.y - error.y );

			//determina se está em uma das regiões de entrada do Storage
			if(validRegion0.Contains(p) && bestFish!=null && bestFish.HasLitter)
			{
				now = true;
				_center = _regions.EntrancePosBegin;
				_point = new Vector3(_center.x,_center.y, 0);
				point2 = _regions.EntrancePosEnd;
			}
			else 
			{
				now = false;
			}


			if(!now && bestFish!=null && EnableInput.Input)
			{
				fish.EnqueeDestination(p);

				if( bestHarbor == null )
					bestHarbor = CollideWithHarbor( newRay );

				if( bestHarbor != null )
				{
					var dist = Algorithm.DistanceToRay( newRay, bestHarbor.RunwayEnd );
					if( dist < bestHarbor.RunwayScale )
					{
						landed = true;
						fish.EnqueeLanding();
					}
				}


			}
			else if(now && EnableInput.Input)
			{
				fish.EnqueeDestination( new Vector2( _point.x - error.x, _point.y - error.y ) );
				fish.EnqueeDestination( new Vector2( point2.x - error.x, point2.y - error.y ) );

				bestFish = null;
			}

		};


		drag.OnDragComplete += ( d )
			=> {

			fish.SetPathSkin( _unselectedPath );
		};
	}

	static Harbor CollideWithHarbor( Ray ray )
	{
		var harbor = Harbor.Instance;
		var dist = Algorithm.DistanceToRay( ray, harbor.RunwayBegin );
		if( dist < harbor.RunwayScale )
		{
			return harbor;
		}
		return null;
	}

}
