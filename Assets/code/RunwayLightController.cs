

using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class RunwayLightController : MonoBehaviour 
{
	[SerializeField] List<Renderer> _lights;
	
	Color _orange = Color.Lerp( Color.red, Color.yellow, .5f );
	Color _green = Color.Lerp( Color.yellow, Color.green, .75f );

	void Start()
	{
		_lights.Sort( (x,y) => y.transform.position.x.CompareTo( x.transform.position.x ) );
		SetFill( 0, _green, _green );
	}
	
	public void DragBegin()
	{
		SetFill( 0, _orange, _orange );
	}

	public void DragUpdate( float f )
	{
		SetFill( f, Color.blue,  _orange );
	}
	
	void SetFill( float f, Color fill, Color empty )
	{
		fill = Color.Lerp( fill, Color.white, .25f );
		empty = Color.Lerp( empty, Color.white, .25f );

		int count = _lights.Count / 2;
		for( int i = 0; i < count; i++ )
		{
			_lights[2*i].material.SetColor( "_TintColor", f > ( ((float)i) / count ) ? fill : empty );
			_lights[2*i+1].material.SetColor( "_TintColor", f > ( ((float)i) / count ) ? fill : empty );
		}
	}
	
	public void DragEnd()
	{
		SetFill( 0, _green, _green );
	}
}