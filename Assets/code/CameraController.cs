using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum ECameraState { GAME, SPLASH };

[System.Serializable]
public class CameraController
{
	[SerializeField] Camera _mainCamera;
	[SerializeField] float _transitionDuartionSeconds = 1;
	[SerializeField] Camera _uiReferenceCamera;
	[SerializeField] Transform _cameraTransform;
	
	[SerializeField] Camera _gameCamera;
	[SerializeField] Camera _splashCamera;

	List<System.Action> _transitionsDestructors = new List<System.Action>();
	
	ICameraState GetCurrentCameraState()
	{
		var camState = new CameraState( _cameraTransform.position, _cameraTransform.rotation, _mainCamera.orthographicSize );
		return camState;
	}
	
	ICameraState GetCameraState( ECameraState camState )
	{
		var camera = _mainCamera;
		switch( camState )
		{
		case ECameraState.GAME: camera = _gameCamera; break;
		case ECameraState.SPLASH: camera = _splashCamera; break;
		}
		var state = new CameraState( camera.transform.position, camera.transform.rotation, camera.orthographicSize );
		return state;
	}
	
	public IEnumerator CameraTransition( IGameState state )
	{
		float durationSeconds = _transitionDuartionSeconds;
		ICameraState initialState = GetCurrentCameraState();

		for( int i = _transitionsDestructors.Count - 1; i >= 0; i-- )
		{
			var tcancel = _transitionsDestructors[i];
			tcancel();
			_transitionsDestructors.RemoveAt( i );
		}
		
		bool cancel = false;
		System.Action cancelation = () => { cancel = true; };
		_transitionsDestructors.Add( cancelation );

		
		float time = 0;
		while( !cancel && time < durationSeconds )
		{
			time += Time.unscaledDeltaTime;
			if( time > durationSeconds )
			{
				var finalCamState = CameraStateInterpolation( initialState, GetCameraState( state.CameraState ), 1 );
				Set( finalCamState );
				break;
			}
			
			var currentCamState = CameraStateInterpolation( initialState, GetCameraState( state.CameraState ), time / durationSeconds );
			Set( currentCamState );
			
			yield return null;
		}
		
		_transitionsDestructors.Remove( cancelation );
	}
	
	public void Set( ICameraState currentCamState )
	{
		_cameraTransform.position = currentCamState.Position;
		_cameraTransform.rotation = currentCamState.Rotation;
		_uiReferenceCamera.orthographicSize = currentCamState.Size;
		_mainCamera.orthographicSize = currentCamState.Size;
	}
	
	public void Set( ECameraState camState ) { Set( GetCameraState( camState ) ); }
	
	ICameraState CameraStateInterpolation( ICameraState a, ICameraState b, float val )
	{
		val = Mathf.Clamp01( val );
		var pos = Vector3.Slerp( a.Position, b.Position, val );
		var rot = Quaternion.Slerp( a.Rotation, b.Rotation, val );
		var size = Mathf.Lerp( a.Size, b.Size, val );
		var camState = new CameraState( pos, rot, size );
		return camState;
	}
}
