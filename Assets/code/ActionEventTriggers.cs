
class ActionEventCapsule: IEventTrigger
{
	System.Action _callback;
	
	public ActionEventCapsule( System.Action callback ) { _callback = callback; }
	public void Trigger() { _callback(); }
	bool IEventTrigger.IsValid { get { return true; } }
	
	public override bool Equals(object obj)
	{
		if( obj is ActionEventCapsule && _callback != null )
		{
			var del = (ActionEventCapsule)obj;
			return _callback.Equals( del._callback );
		}
		return base.Equals(obj);
	}
	public override int GetHashCode() { return _callback.GetHashCode(); }
}

class ActionEventCapsule<T>: IEventTrigger<T>
{
	System.Action<T> _callback;
	
	public ActionEventCapsule( System.Action<T> callback ) { _callback = callback; }
	public void Trigger( T t ) { _callback( t ); }
	public bool IsValid { get { return true; } }
	
	public override bool Equals(object obj)
	{
		if( obj is ActionEventCapsule<T> && _callback != null )
		{
			var del = (ActionEventCapsule<T>)obj;
			return _callback.Equals( del._callback );
		}
		return base.Equals(obj);
	}
	public override int GetHashCode() { return _callback.GetHashCode(); }
}

class ActionEventCapsule<T,K>: IEventTrigger<T,K>
{
	System.Action<T,K> _callback;
	
	public ActionEventCapsule( System.Action<T,K> callback ) { _callback = callback; }
	public void Trigger( T t, K k ) { _callback( t, k ); }
	public bool IsValid { get { return true; } }
	
	public override bool Equals( object obj )
	{
		if( obj is ActionEventCapsule<T,K> && _callback != null )
		{
			var del = (ActionEventCapsule<T,K>)obj;
			return _callback.Equals( del._callback );
		}
		return base.Equals(obj);
	}
	public override int GetHashCode() { return _callback.GetHashCode(); }
}

public class DelegateConditionalEventListener<T>: IEventTrigger<T>
{
	System.Action<T> _callback;
	System.Func<bool> _condition;
	
	public DelegateConditionalEventListener( System.Action<T> callback, System.Func<bool> condition ) { _callback = callback; _condition = condition; }
	public void Trigger( T t ) { _callback( t ); }    
	public bool IsValid { get { return _condition(); } }
	
	public override bool Equals( object obj )
	{
		if( obj is DelegateConditionalEventListener<T> && _callback != null && _condition != null )
		{
			var del = (DelegateConditionalEventListener<T>)obj;
			return _callback.Equals( del._callback ) && _condition.Equals( del._condition );
		}
		return base.Equals(obj);
	}
	
	public override int GetHashCode() { return _callback.GetHashCode() + _condition.GetHashCode(); }
}