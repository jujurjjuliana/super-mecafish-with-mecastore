﻿using UnityEngine;
using System.Collections;

public class AttachedComponent<T> : MonoBehaviour where T : Component
{
    private T _attachedWidget;
    
    protected T Attached
    {
        get
        { 
            if( _attachedWidget == null )
                _attachedWidget = GetComponent< T >();
            
            return _attachedWidget; 
        } 
    }
}

public class CachedComponent<T> where T : Component
{
    private GameObject _gameObject;
    private T _cache;
    
    public T Component
    {
        get
        { 
            if( _cache == null )
                _cache = _gameObject.GetComponent<T>();

            return _cache; 
        }
    }

    public CachedComponent( GameObject go )
    {
        _gameObject = go;
    }
}

public class CachedChildComponent<T> where T : Component
{
    private GameObject _gameObject;
    private T _cache;

    public T Component
    {
        get
        {
            if( _cache == null )
                _cache = _gameObject.GetComponentInChildren<T>();

            return _cache;
        }
    }

    public CachedChildComponent( GameObject go )
    {
        _gameObject = go;
    }
}